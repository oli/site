---
title: oli who?
layout: en
category: eng
---

![profile photo](/img/foto-perfil2.jpeg =300x){.right .foto-perfil} my name is Oli Maia; I'm a Brazilian writer, artist and sometimes the worse artisan in the history of artisanship. you can get in touch via [telegram](https://www.t.me/olimaia) or [email](mailto:oli@olimaia.net).

## find me on the internet

- **mastodon [pt-br]**: [cuscuz.in/@oli](https://cuscuz.in/@oli)
- **videos**: [share.tube](https://share.tube/accounts/olivia)
- **photos**: [fotos.olimaia.net](https://fotos.olimaia.net)

## art

everything available on this website is released under a [CC-BY-SA license](https://creativecommons.org/licenses/by-sa/4.0/deed), which means you can download all drawings you want, print them and hang them on your wall, send them over to your friends and family or even use them on your own work, as long as you give me proper credit and don't change the license.

most of my website is in _Portuguese_ but you can still check out my [inktober drawings](/arte/inktober), or perhaps download some [free zines](/zine) and practice your language skills.

- [![rabiscos](/img/rabiscos-p.png) art](/en/art)
- [![originais](/img/rabiscos-originais.jpg) original artworks](/en/originals)
- [![inktobers](/img/ink-arte.png) inktobers](/arte/inktober) (pt)
{.rabiscos-listagem}

## support my work

if you like my work and would like to send me a gift or support me, you can do so on:

- **liberapay**: [liberapay/nyex](https://liberapay.com/nyex)  
- **paypal**: [donate in BRL](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D3MSR4A6T4F2J&source=url) or ask me for my paypal email to donate directly from your account in any currency you prefer
