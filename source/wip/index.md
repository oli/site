---
title: escrita
---

![gráfico de contagem de palavras do livro "O diabo não há", cuja escrita começou em setembro de 2023](/img/nw_stat.png)

contagem de palavras atual: **160854**
atualizado em: 28/11/2024

[[script para geração do gráfico](https://codeberg.org/oli/novel-progress)]{.right .small}
