---
title: quem?
---

![foto de perfil](/img/foto-perfil2.jpeg =300x){.right .foto-perfil} meu nome é Oli Maia; sou artista, escritor e rabiscador. nasci em São Paulo e, em agosto de 2013, deixei o trabalho como professor e fui embora de São Paulo com uma mochila nas costas. depois de dois anos e dois pares de botas entre os Andes e os Alpes, me instalei no interior da Bahia, na cidade de Lençóis.

## entre em contato

[e-mail](mailto:oli@olimaia.net) | [telegram](https://www.t.me/olimaia)

## me encontre na internet

- **mastodon**: [mastodon.com.br/@oli](https://mastodon.com.br/@oli)
- **vídeos**: [share.tube](https://share.tube/accounts/olivia)
- **fotos**: [fotos.olimaia.net](https://fotos.olimaia.net)

## literatura

existo na internet desde 2002 e comecei a publicar literatura em 2006. meus dois últimos livros, [**TRÉGUA**](/livros/tregua) e [**NORTE**](/livros/norte), foram publicados via financiamento coletivo no catarse. [veja aqui](/livros) os romances e contos que já publiquei.

## arte & zine & rabiscos

os [zines](/zine) e [meu trabalho de ilustração](/arte) estão disponíveis como download gratuito do arquivo em alta resolução sob [licença creative commons CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). bora espalhar arte.

publico vídeos das  em timelapse (velocidade acelerada) no meu [canal de arte no peertube](https://share.tube/video-channels/art_things).

## apoie meu trabalho

- **catarse**: [catarse/olimaia](https://catarse.me/olimaia)  
- **liberapay**: [liberapay/nyex](https://pt.liberapay.com/nyex)  
- **paypal**: [página de doação única](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D3MSR4A6T4F2J&source=url)  
