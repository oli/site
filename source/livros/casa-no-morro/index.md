---
title: a casa no morro
layout: livro
photos:
  - /img/capas_livros/casanomorro.png
páginas: 42
editora: Edição digital publicada pela Editora Draco
data: set/2009
goodreads: https://www.goodreads.com/book/show/30461386-a-casa-no-morro
---

## sinopse

Um assassinato na porta de um bar leva os investigadores Pedro e Iuri a uma casa abandonada em Caieras, onde deparam com uma história muito mal contada sobre uma pistola da 2ª guerra mundial.

## comprar

- [livraria cultura](http://www.livrariacultura.com.br/p/a-casa-no-morro-100889797)
- [e-book kindle](https://www.amazon.com.br/casa-morro-Contos-Drago-ebook/dp/B01DUPKEHU/)
- [e-book kobo](https://store.kobobooks.com/pt-br/ebook/a-casa-no-morro)
- [google livros](https://play.google.com/store/books/details/Olivia_Maia_A_casa_no_morro?id=37LkCwAAQBAJ)

## resenhas

— [A casa no morro e Jamais o inexistente sorriso, de Olivia Maia](https://literaturapolicial.com/2016/05/16/a-casa-no-morro-e-jamais-o-inexistente-sorriso-sao-contos-policias-da-paulistana-olivia-maia/), por Josué de Oliveira
