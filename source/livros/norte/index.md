---
title: norte
id_compra_impresso: NORTE - impresso
id_compra_digital: NORTE - ebook
layout: livro
photos:
  - /img/capas_livros/norte.png
páginas: 184
editora: Edição do autor
isbn: 978-65-00-75354-7
formato: 14 x 21
data: out/2023
goodreads: https://www.goodreads.com/book/show/200676591-norte
preco_impresso: 'R$ 50.00'
preco_digital: 'R$ 9.90'
---

## sinopse

No espaço entre Buenos Aires, os desertos de Catamarca e a Patagônia aos pés da cordilheira andina, Téo Miranda está deslocado de seu território habitual. Acostumado a investigar adultérios na São Paulo urbana, o detetive particular descobre-se em paragens remotas, contra o vento e a poeira, na busca por um tio desaparecido há mais de trinta anos.

Marcada por curvas imprevistas e a aridez da _Ruta Nacional 40_, a investigação se torna ainda mais difícil pela companhia insistente de Gustavo, com quem Téo mantém um relacionamento incerto. O detetive logo se dá conta de que para encontrar o tio precisa compreender seus motivos, mas a tarefa o obrigará a enfrentar suas próprias inseguranças — ou correr o risco de se perder nesse eterno meio de caminho no rumo do _fin del mundo_. 

## comprar

Você pode comprar seu exemplar assinado ou e-book sem DRM aqui, diretamente comigo. Entre em contato via [e-mail](mailto:oli@olimaia.net) ou [telegram](https://www.t.me/olimaia).
