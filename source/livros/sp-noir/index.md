---
title: são paulo noir
layout: livro
photos:
  - /img/capas_livros/spnoir.jpg
páginas: 272
editora: Editora Casa da Palavra
isbn: 978-8544104842
data: nov/2016
infoextra: Coletânea de contos editada por Tony Bellotto.
goodreads: https://www.goodreads.com/book/show/33399633-s-o-paulo-noir
---

## sinopse

Paulicéia é tema do novo livro da série “Noir” Uma seleção de histórias surpreendentes onde a cinzenta São Paulo, mais do que um simples cenário, torna-se personagem. Contos policiais que retomam a atmosfera noir imortalizada por Hollywood em filmes como “O falcão maltês”, escritos por nomes como Marcelo Rubens Paiva, Mario Prata, Jô Soares, Drauzio Varella, Ferréz, Vanessa Barbara, entre outros.

## comprar

- [livraria cultura](http://www.livrariacultura.com.br/p/livros/literatura-nacional/ficcao-policial/sao-paulo-noir-46398024)
- [livraria saraiva](http://www.saraiva.com.br/sao-paulo-noir-9387640.html)
- [amazon](https://www.amazon.com.br/gp/product/8544104843/)
- [e-book kindle](https://www.amazon.com.br/S%C3%A3o-Paulo-Noir-Suspense-protagonista-ebook/dp/B06VVYHZZB)
- [e-book kobo](https://www.kobo.com/br/pt/ebook/sao-paulo-noir-suspense-dramas-urbanos-e-uma-grande-protagonista-a-cidade-da-garoa)

## resenhas

– [São Paulo noir](https://www.goodreads.com/review/show/1845524727?book_show_action=false), por Jana Bianchi
