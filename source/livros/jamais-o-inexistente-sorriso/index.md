---
title: jamais o inexistente sorriso
layout: livro
photos:
  - /img/capas_livros/jamais.png
páginas: 56
editora: Edição digital publicada pela Editora Draco
data: set/2010
goodreads: https://www.goodreads.com/book/show/30461385-jamais-o-inexistente-sorriso
---

## sinopse

A primeira investigação do delegado Daniel começa numa rua sem saída: uma moça sem nome e sem passado internada na ala psiquiátrica de um hospital. Intrigado, Daniel sente-se obrigado a buscar uma resposta nos submundos da cidade, sem se dar conta de que sua própria vida pode também estar em risco.

## comprar

- [livraria cultura](http://www.livrariacultura.com.br/p/jamais-o-inexistente-sorriso-100889815)
- [e-book kindle](https://www.amazon.com.br/Jamais-inexistente-sorriso-Contos-Drago-ebook/dp/B01DUPKEH0/)
- [e-book kobo](https://store.kobobooks.com/pt-br/ebook/jamais-o-inexistente-sorriso)
- [google livros](https://play.google.com/store/books/details/Olivia_Maia_Jamais_o_inexistente_sorriso?id=urLkCwAAQBAJ)

## resenhas

— [A casa no morro e Jamais o inexistente sorriso, de Olivia Maia](https://literaturapolicial.com/2016/05/16/a-casa-no-morro-e-jamais-o-inexistente-sorriso-sao-contos-policias-da-paulistana-olivia-maia/), por Josué de Oliveira
— [Jamais o inexistente sorriso – Olivia Maia](http://www.literotopo.com.br/2013/02/jamais-o-inexistente-sorriso-olivia-maia.html), por Emanuel Campos
