---
title: " Serra da Capivara: primeiro dia de caminhada"
date: 2013-09-20 06:30:20
---

saímos do hotel às sete e meia da manhã levando nas costas um litro e meio de água e o lanche com um suco para o almoço. o parque fica a pelo menos uns 30 km de SRN; depende da entrada e do percurso que se quer fazer. nosso primeiro destino foi o Desfiladeiro da Capivara, na Serra da Capivara (pelo que entendi são quatro serras dentro do parque, que é bem grande; sem contar a quantidade de sítios arqueológicos, que é um troço absurdo).

passando a guarita seguimos por um caminho de carro, de onde já deu para ver os primeiros mocós (o que mais tem naquele parque é mocó) e uma turma de macacos-prego lanchando na beira da estrada. 

enfim deixamos o carro para a primeira parte da caminhada, pelo circuito do baixão das vacas (onde antigamente as vacas pastavam). de lá fomos nos metendo nas tocas com as pinturas rupestres. perdi a conta de quantos sítios visitamos e principalmente de quantos mocós surgiram no meio das pedras. quando a gente chega perto eles ficam paradinhos, espiando de rabo de olho, muito certos de que ninguém está vendo eles ali. 

fomos subindo a serra para visitar os sítios mais altos e para ver a paisagem. caminhar com mais de trinta graus e pouca sombra não é fácil, ainda mais com a água e o lanche nas costas. e tem que regular um pouco a água, porque não tem jeito de reabastecer pelo caminho.

terminamos a manhã com o circuito dos veadinhos azuis (umas pinturas em um tom azulado, diferente da maioria das pinturas que é feita num tom avermelhado) e logo fomos descendo de volta para o baixão das vacas, de onde seguimos para o carro e uma mesa debaixo de uma sombra para almoçar.

gostei foi de um lugar chamado inferno, uma passagem que só tem luz quando o sol está bem alto, e no resto do dia vira um breu assustador, segundo nos explicou o Leandro (por isso o nome simpático). a passagem termina numa gruta e tinha era um monte de morcego ali num canto mais escuro.

na volta o Leandro ainda nos levou para conhecer a Cerâmica Serra da Capivara (no município de Coronel José Dias, onde também está a maior parte do parque), que é de onde saem esses pratos e copos de cerâmica que estão à venda no Pão de Açúcar, na Tok Stok, no centro de Fortaleza etc. o Marcos, que explicou como funciona o trabalho feito por lá, disse que São Paulo é o maior mercado deles, mas de qualquer forma eles vendem para o país todo e até exportam alguma coisa. ali também funciona um albergue. 

puf puf. cansei. na quinta-feira fizemos um intervalo para ir ao banco (pausa essa frustrada pela greve) e descansar um pouco. agora sexta e sábado tem mais parque e caminhada.
