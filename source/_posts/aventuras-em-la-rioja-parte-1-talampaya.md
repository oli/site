---
title: " aventuras em la rioja, parte 1: talampaya"
date: 2014-09-27 23:16:54
---

com o primeiro dia inteiro em Villa Unión tomamos um transfer até o parque nacional Talampaya, que fica uns bons quilômetros de distância do povoado. no parque, sem carro, não temos muita opção senão tomar uma excursão para conhecer o cânion. fomos numa em um caminhão adaptado com uma parte descoberta em cima. de lá dá pra ir tirando fotos sem brigar com janela suja.

na verdade foi massa porque milhões de bichinhos: guanacos, ñandus e uns tipos de lebres e roedores cujo nome eu já esqueci. o esquema excursão é mei chato porque aquele monte de gente se jogando na sua frente pra tirar uma selfie e desde do ônibus sobe no ônibus ronc.

e claro que o cânion: enorme enorme; nas fotos a gente perde essa noção da grandeza da coisa. e na verdade mesmo quando estamos lá, ao vivo. quando calha de aparecer um humanozinho no pé do paredão a gente tem a perspectiva do tamanho.

a excursão incluiu uma parada com vinho branco e suco e snacks no meio do cânion, o que me pareceu bem desnecessário, mas enfim. lá tem também um tal sendero del triasico com uns dinos bem safados, mas com um cenário de quase deserto que faz o percurso interessante.

aí que terminada a excursão ficamos ali matando tempo e se escondendo do sol pra pegar carona com uns guias que voltavam de carro pra Villa Unión. de manhã fazia um frio terrível e ali passado meio-dia um calor de mais de 30 graus. o guia depois na volta me esclareceu o estranho sotaque riojano (e eu descobriria depois que está presente praticamente em todo o norte e também em San Juan) que não pronuncia o R de, por exemplo, _sierra_. eles dizem algo entre _sieja_ e _sieza_ (no caso z e j da pronúncia do português). incrível como a gente logo se acostuma e volta a entender tudo. também altos papos sobre política e educação e as diferenças entre Brasil e Argentina.
