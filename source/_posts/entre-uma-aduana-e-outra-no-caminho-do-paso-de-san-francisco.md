---
title: " entre uma aduana e outra no caminho do paso de san francisco"
date: 2014-11-16 08:00:02
---

dois dias em país nenhum, metidos num limbo diplomático na cordilheira dos andes. teoricamente é solo argentino, mas o passaporte dizia outra coisa. primeiro dia Fernando basicamente dormiu, eu caminhei um pouco enfrentando o vento pra encontrar umas lagoas, uns passarinhos e muitas vicuñas. 

a puna não me agarrou e talvez o corpo tenha algum tipo de memória porque no último mês estava nessa de sobe e desce da altitude sem nenhuma periodicidade. cansaço sim, claro, mas dava pra caminhar sem sentir demais as consequências da falta de oxigênio. só os músculos e o fôlego reclamavam um pouco, mas a cabeça e o estômago estavam contentes. segundo dia Fernando se sentia melhor e nos metemos a subir uma montanha em frente ao refúgio, uma que alcança quase 4500 metros de altitude. nos acompanhou uma cachorrinha filhotona que foi reclamando quase todo o percurso e só sossegava quando alguém se sentava pra ela subir no colo ou quando a gente lhe dava bolacha de água e sal. 

não subimos tudo e fomos descendo por onde não tinha caminho em direção a uma lagoa cheia de flamingos, que obviamente voaram pra longe quando perceberam que nos aproximávamos. 

as distâncias na altitude são infinitas. aí que chegamos e o Fernando estava apunado outra vez, todo estragado, e dormiu o resto do dia com o corpo todo xingando a falta de oxigênio. eu sem livro que meu kobo quebrou fiquei ali tomando mate no sol, meditando e às vezes perturbando o Fernando pra ele tomar água (os montanhistas insistiam _diz pra ele tomar água_ e ele todo desmaiado e eu pensando _mas vou meter água pela orelha do rapaz que ceis querem que eu faça?_). pensamos que podia ser melhor voltar pra Fiambalá mas não tinha ninguém ali que ia voltar e na aduana não passava alma viva. só o vento. restava encarar a noite e no dia seguinte descobrir pra onde nos mandava a sorte.
