---
title: " dia em trânsito: mina clavero via carlos paz"
date: 2014-12-13 06:28:56
---

tinha companhia pra um almoço em Carlos Paz e era oportunidade pra rever a Graça, minha avó postiça que vive por lá. na quinta-feira dia 4 saí cedinho de Valle Hermoso e na pista tomei um Ciudad de Córdoba mei feio. liguei pra Graça e disse que estava a caminho. pode parecer meio tonto, mas uma das coisas que eu mais gosto de viajar são esses momentos em que estou com a mochila e ainda não tenho definido onde vou dormir, e estou no meio do caminho entre um lugar e outro. caminhar com a mochila nas costas sabendo que tenho ali tudo o que eu preciso. mudar de ideia, mudar de rumo. não estar amarrada a nada. deve ser um exagero, mas me dá uma alegriazinha. Graça me buscou na rodoviária de Carlos Paz e na casa dela conversamos sobre o desastre que são os homens argentinos, cheios de voltas, de não sei não conheço nunca vi que dia é hoje etc. aparentemente é um consenso; amiga de Tucumán disse o mesmo. deos. final da manhã voltei à rodoviária pra encontrar um amigo suíço (esses amigos viajantes que a gente conhece pela internet e de repente calha de estar mais ou menos no mesmo lugar e não custa conhecer pessoalmente). tomamos o rumo do centro conversando sobre lugares e pessoas e viagens e montanhas até encontrar um lugar pra tomar uma cerveja e comer alguma coisa. matamos a tarde por ali até a hora que eu tinha que tomar o ônibus pra Mina Clavero; queria tomar o das 17h pra ainda conseguir ver o caminho, que segundo minhas fontes era mui lindo. busquei mochila, me despedi da Graça e ganhamos carona de volta à rodoviária. meio em cima da hora: o ônibus só tinha lugar no fundo. _y bueno_. o amigo suíço tomou o rumo de Córdoba e eu me meti num Coata mei sem vergonha com destino às Altas Cumbres.
