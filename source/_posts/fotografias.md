---
title: " fotografias"
date: 2015-03-28 17:37:20
---

há mais ou menos dois anos eu estava na praia sem minha câmera fotográfica lambida pelo mar e pensando difícil ficar sem uma câmera, que difícil não observar o mundo buscando um melhor ângulo pra uma foto e a frustração de não ter como registrá-lo. tiro cada vez menos fotos. os ângulos vão se repetindo, as paisagens se não são parecidas são grandes demais, lindas demais, pra uma só foto. mudou qualquer coisa importante no meu olhar pro mundo. e a função das fotografias na minha vida. parece.
