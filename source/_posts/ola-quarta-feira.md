---
title: " olá, quarta-feira"
date: 2009-04-23 01:44:16
---

o problema entre a vida offline e a vida online é que elas se conectam só quando há afinal cabos e transmissão de dados, olha aqui essa foto, ouve aqui essa música, ou então sou só eu que estou ainda muito distante desse ideal de supermegacelular com programas que tiram foto e baixam música e bota online e traz offline ao mesmo tempo enquanto toma um picolé de manga e escreve um post no blog e faz dois updates relevantes no twitter conversando com três pelo gtalk. ou o problema mesmo são esses momentos em que ficamos entre uma coisa e outra, sem ser uma coisa nem outra, que afinal é um desses dilemas antigos da humanidade e talvez signifique que o mundo roda roda roda e continuamos os mesmos, sempre.
