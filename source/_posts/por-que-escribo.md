---
title: " por qué escribo"
date: 2011-01-20 00:15:00
---

> Porque no sé bailar el tango, tocar un instrumento musical como la celesta o el _glockenspiel,_ resolver problemas de matemáticas superiores, correr una maratón en Nueva York, trazar las órbitas de los planetas, escalar montañas, jugar al fútbol, jugar al rugby, excavar ruinas arqueológicas en Guatemala, descifrar códigos secretos, rezar como un moje tibetano, cruzar el Atlántico en solitario, hacer carpintería, construir una cabaña en Algonquin Park, conducir un avión a reacción, hacer surf, jugar a complejos videojuegos, resolver crucigramas, jugar al ajedrez, hacer costura, traducir del árabe y del griego, realizar la ceremonia del té, descuartizar un cerdo, ser corredor de Bolsa en Hong Kong, plantar orquídeas, cosechar cebada, hacer la danza del vientre, patinar, conversar en el lenguaje de los sordomudos, recitar el Corán de memoria, actuar en un teatro, volar en dirigible, ser cinematógrafo y hacer una película, en blanco y negro, absolutamente realista de Alicia en el País de las Maravillas, hacerme pasar por un banquero respetable y estafar a miles de personas, deleitarme con un plato de tripas _à la mode de Caën_, hacer vino, ser médico y viajar a un lugar devastado por la guerra y tratar con gente que ha perdido un brazo, una pierna, una casa, un hijo, organizar una misión diplomática para resolver el problema del Medio Oriente, salvar náufragos, dedicar treinta años al estudio de la paleografía sánscrita, restaurar cuadros venecianos, ser orfebre, dar saltos mortales con o sin red, silbar, decir por qué escribo. (Alberto Manguel)

via [elpais.com](http://www.elpais.com/articulo/portada/escribo/elpepusoceps/20110102elpepspor_9/Tes)

o jornal El País perguntou a vários escritores por que eles escrevem. a pergunta de sempre. a resposta do Alberto Manguel é a melhor de todas. ditto.
