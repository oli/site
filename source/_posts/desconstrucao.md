---
title: " desconstrução"
date: 2010-03-17 17:57:28
---

> I can usually sniff out a book that's been written by a creative writing student within a few pages; there will be no plot to speak of and each sentence will have been polished so many times it will be dead.
> 
> [daqui.](http://www.guardian.co.uk/commentisfree/2010/mar/17/good-read-novels-genre-fiction?utm_source=twitterfeed&utm_medium=twitter)

curioso que muito da literatura brasileira contemporânea é isso.

de certa forma, a gente pode pensar que os artistas (de um modo bem mais amplo) brasileiros criaram hábito e gosto pela desconstrução, mas esquecem de se perguntar o que exatamente estão desconstruindo. ou, ainda: desconstruímos os outros. os americanos, os estrangeiros, a cultura de massa. e nessa briga toda perdeu-se o meio termo.

a gente anda precisando do meio-termo. 

mesmo que seja pra desconstruir depois.
