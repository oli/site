---
title: " primeiro dia de São Raimundo Nonato"
date: 2013-09-19 19:19:41
---

porque a segunda-feira não conta; chegamos em SRN por volta de oito da noite, tomamos um táxi da rodoviária até o Hotel Real e conseguimos um quarto standard, sem ar condicionado, por um preço camarada. depois só fizemos encontrar uma pizzaria (sim, sim, e pizza mui boa com pizzaiolo que trabalhou por 18 anos em São Paulo), jantar e dormir.

ainda à noite eu tinha conversado por telefone com o guia Giordano, que nos indicou o guia Rafael, com quem conversamos na terça-feira pela manhã. no final das contas ele acabou nos indicando o guia Leandro (ufa), que tem carro e poderia nos fazer um preço melhor por causa disso (transporte para o parque é um problema e os táxis cobram muito caro). combinamos três dias de passeio com ele. enfim resolvemos enfrentar a caminhada de 3 km até o Museu do Homem Americano. fomos pelo atalho explicado pelo Leandro. depois de pedir informação de direção na padaria onde paramos para comprar água (explicar direções não é bem o forte do pessoal por aqui), um sujeito veio atrás de nós para nos acompanhar, dizendo que morava no caminho para o museu e que fôssemos com ele e depois ele podia nos explicar o resto do percurso. depois ainda disse que a gente passasse por lá na volta, para tomar um café ou uma água gelada. _aquela casa azul ali em cima, é só perguntar pela casa do Cícero_. o pessoal aqui no Piauí até agora nos foi sempre muito solícito, mais até que os cearenses, que são solícitos ao modo deles (parece que estão brigando com você, mas na verdade estão querendo ajudar). a gente que é bicho de cidade grande chega a estranhar um pouco. a caminhada até o museu não é longa mas debaixo do sol forte sem nenhuma sombra e trinta e tantos graus claro que qualquer caminhada parece infinita. 

chegamos primeiro na UNIVASF (Universidade Federal do Vale do São Francisco). a universidade tem os cursos de Arqueologia e de Ciências Naturais, mas estava em recesso, vazia. uma funcionária nos contou que os alunos estão em trabalhos de campo. aproveitamos para encher as garrafas d'água antes de seguir para o museu.

custa 10 reais para visitar o museu. ele é pequeno: tem quatro ambientes. mas ficamos um bom tempo vendo um vídeo num telão na sala maior, lendo os textos nas paredes, vendo as imagens das escavações e enfim os artefatos e ossadas e réplicas expostos (e aproveitando o ar condicionado). lembro de ter ouvido falar sobre a Niède Guidón pela primeira vez quando esta na quinta ou na sexta série. mas sempre o Piauí pareceu um troço muito distante e de qualquer forma homens pré-históricos e sítios arqueológicos são um troço mais distante ainda.

a volta pareceu ainda mais quente. passava de uma da tarde e a primeira sombra só apareceu para além do meio do caminho; ali também cruzamos com uma senhora que comentou sobre o calor e me pediu ajuda para ler uma nota fiscal, e nos foi acompanhando até a cidade, um pouco conversando com a gente, um pouco falando consigo.

depois de encontrar um lugar para almoçar achamos melhor descansar o resto do dia. na sombra.
