---
title: " Billy Budd: rodeios narrativos\t\t"
date: 2010-05-05 17:49:25
---

lendo _Billy Budd, marinheiro_, do Herman Melville, pensei o quando uma narrativa desse tipo não teria vez nos dias de hoje. página 50, de 135, e o narrador ainda está descrevendo o cenário político e os personagens que serão importantes para a história. mas a história mesmo, ainda nada. sei, pela sinopse na contra-capa, que Billy Budd será injustamente acusado de incitar um motim a bordo.

tudo o que ouvimos hoje é: "agarre o leitor nas primeiras páginas", "vá direto ao conflito" etc. não nego, eu daria (e dou) esse mesmo tipo de sugestão pra quem está querendo começar a escrever alguma coisa.

mas fico pensando o quanto não perdemos, por possibilidades. claro que, vale pensar, também, esse tipo de narrativa por si já tem, digamos, pouca vez nos dias de hoje, e todo o trabalho minucioso de descrição e apresentação de personagens que Melville faz não seria, em seus termos, tão pertinente a uma narrativa moderna. _Billy Budd_ seria, se tanto, um conto. ainda assim, um tanto se perde, justamente porque essa demora em se começar a história, em rodeios por não pecar pelo excesso de objetividade, tem um objetivo maior de fazer com a história em si valha algo mais do que o simples relato de um caso de marinheiro.

e aí, sim, há coerência nessas 50 páginas de apresentação de motins anteriores e personagens que ainda não significam muita coisa ao leitor. há uma significação maior para o que vem depois. sem o cenário político -- os recentes episódios de motins na marinha britânica -- (e pro próprio Melville se tratava de algo acontecido no passado) penso que a história perderia talvez sua força.

e fico me perguntando: como seria contada uma história como a de _Billy Budd_ hoje?
