---
title: " caminhada na quebrada de los condoritos"
date: 2014-12-15 20:32:00
---

acordar foi fácil. depois que a coisa foi se complicando. tomamos o ônibus que saía 8h10 com destino à Córdoba e descemos na pista no meio das Altas Cumbres, na entrada do parque.

até o centro de visitantes eram dois quilômetros (vai contando). nos registramos e escutamos a explicação do caminho: uma caminhada de 6 km (a moça do parque dizia duas horas de caminhada mas é um exagero) e chegaríamos a uma bifurcação, de onde se poderia ir ao _balcón sur_ ou à quebrada (o rio) e mais adiante ao _balcón norte_. decidimos caminhar e descobrir depois que rumo a gente ia tomar. não tem sombra, mas o caminho é lindo. esses arbustos com esse verde amarelo quase dourado e o céu azul, as pedras cinzentas por todos os lados.

na bifurcação tomamos o caminho da esquerda ao _balcón sur_: uma descida bem íngreme que foi lentamente destruindo meus joelhos, como convém. pra piorar no dia anterior eu tinha topado um dedo do pé numa pedra do _nido_ e a bota me estava torturando um tanto. alcançamos o mirante e não vimos nenhum condor. puf. 

então subimos. puf puf puf. e tomamos o outro caminho pra descer à quebrada. puf. joelho já estava mei indignado porque não queria mais descer nada e também o Oliver começou a sofrer com um dedo do pé. chegamos à quebrada no tempo em que uma nuvem bem grande e amiga cobria o sol. foi escolher um lugarzinho pro nosso piquenique, tirar as botas, comer e ficar um pouco à toa. 

dali vimos uns condores bem distantes e decidimos que não tínhamos bem pique nem tempo pra subir até o _balcón norte_ porque joelho, dedo, pé; tudo estragado. também que foram dois dias anteriores de um monte de caminhada e o corpo uma hora diz _chega, né_. eram duas e meia quando começamos a voltar, pensando se quem sabe conseguíamos o ônibus das 16h. claro que não conseguimos. quase no centro de visitantes estávamos os dois caminhando meio torto. alcançamos a estrada um pouco antes das 17h e o próximo ônibus passaria por volta de 18h. nos restou pedir carona e ver a turma passando a toda velocidade e nem percebendo que a gente estava ali com o dedo erguido. parou um carro que só cabia um e depois de uns quarenta minutos (o sol já tinha voltado) parou um rapaz indo a Villa Dolores que nos foi contando de todas as suas aventuras e desventuras amorosas ao longo do caminho. da entrada de Mina Clavero ainda tomamos um táxi porque tinha acabado perna e pé. inclusive nem sobrou muita pilha pra cozinhar: tomamos umas cervejas e pedimos pizza e devoramos toda a pizza. bem mortinhos.
