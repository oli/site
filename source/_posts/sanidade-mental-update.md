---
title: " sanidade mental update"
date: 2016-09-01 16:15:11
---

um remédio a menos na fila dos remédios diários, um efeito colateral a menos e uma vaga sugestão do psiquiatra de que ele está fazendo o possível pra evitar me receitar logo um rivotril. não deve ser pra tanto.
