---
title: " os preparativos"
date: 2013-07-01 16:51:31
---

comprar mochila cargueira maior, comprar saco de dormir, trocar o mp3 player que já funciona mui mal (o ipod de 2006 só funciona no carro via usb porque o contato dos fones de ouvido morreu) por um mais simples e mais barato (acabei adaptando um nokia xpressmusic com um cartão de 16gb), comprar tecido pra fazer uns separadores de bagagem que a mama vai confeccionar, organizar o estojo com material de desenho, escolher um plano de saúde com abrangência nacional que não custe metade do orçamento mensal, trocar o plano do celular pra um mais barato e pertinente, ir ao dentista pra fazer limpeza e cuidar (finalmente) da atm, fazer fisioterapia porque essa dor nas costas que não passa e segundo o ortopedista é uma “lesão”, conversar com a gerente do banco, atualizar documentos e cadastros com meu nome de solteira (tirar um passaporte novo), digitalizar documentos e fazer cópias autenticadas dos mais importantes, fazer um estoque de lentes de contato, fazer uma lista de bagagem e ver se cabe tudo na mochila, escolher os livros que vou levar. puf puf. temos o mês de julho e ainda a primeira semana de agosto. agora sim. 
