---
title: " epígrafes"
date: 2011-02-20 00:18:00
---

as duas epígrafes para o livro que estou escrevendo:

_Tudo seria como uma inquietação, uma falta de sossego, um desarranjo contínuo, um território onde a causalidade psicológica cederia, desconcertada, e esses fantoches se destruiriam ou se amariam ou se reconheceriam sem suspeitar demasiado de que a vida procura trocar a clave deles e através deles e por eles, de que uma tentativa ainda pouco concebível nasce no homem da mesma forma como outrora foram nascendo a clave-razão, a clave-sentimento, a clave-pragmatismo. Que a cada sucessiva derrota há uma aproximação da mutação final, e que o homem não é, mas procura ser, projeta ser, algemado entre palavras e comportamento e alegria salpicada de sangue e outras retóricas como esta._

Julio Cortázar  
_Jogo da amarelinha_, capítulo 62

  
_Assim, profissionalmente, assim em todos os planos; aquela que teme a violação profunda de sua vida, a invasão na ordem obstinada de seu abecedário, Hélène que só entregou seu corpo quando tinha a certeza de que não a amavam, e só por isso, para deslindar o presente e o futuro, para que ninguém subisse depois para bater à sua porta em nome dos sentimentos._

Julio Cortázar  
_62 Modelo para armar_
