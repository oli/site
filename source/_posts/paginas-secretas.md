---
title: páginas secretas
date: 2021-11-24 09:58:55
---

à parte que talvez finalmente eu tenha conseguido fazer um script de post novo que realmente funciona (bash me odeia), fiz também esses dias uma página _não-listada_ que seria talvez secreta mas ei-la:

[oliviamaia.net/log](https://oliviamaia.net/log)

basicamente um pseudo microblogging interno; o script que estou usando manda o texto pro [mastodon](https://masto.donte.com.br/@olivia) também, mas eles vão ficando registrados nessa página. não tem rss, não tem utilidade, não tem nada de interessante.

completamente desnecessário.
