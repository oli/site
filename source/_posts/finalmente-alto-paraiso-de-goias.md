---
title: " finalmente Alto Paraíso de Goiás"
date: 2013-10-17 19:36:20
---

continuando a saga dos ônibus, algumas imagens do caminho no ônibus "convencional" que nos levou ontem de Brasília a Alto Paraíso de Goiás.

não foi de todo mal viajar um pouco num ônibus que não fosse com ar condicionado congelando a alma (ainda mais agora que estou sem minha blusa quentinha). numa das paradas em rodoviária (claro que ele parava em todas as cidades do caminho), o motorista comprou uma fatia gorda de queijo e mui gentilmente nos ofereceu um pedaço. foi comendo alegremente estrada acima (na verdade a impressão é que a viagem foi feita 70% do tempo em primeira marcha ladeira acima). cinco horas de viagem. chegamos por volta de oito da noite. a dona do Hostel Catavento mui solícita nos buscou na rodoviária. o albergue fica meio afastado do centro mas é caminhada de 15 minutos (com uma ladeira). descansamos numa cama grande e quentinha depois de duas noites dormindo mei torto em poltrona de ônibus. ufas. hoje amanheceu frio e chovendo e ficou frio e chovendo praticamente todo o dia. bom para ficar à toa, ainda mais aqui no albergue que tem gatos, internet e vários cantinhos aconchegantes. logo de manhã o Fabio se acomodou no camping Eco Nóis para fazer alguns dias de semi-wwoofing.

tem ainda as trilhas para as cachoeiras ainda que a maioria precise carro (duas delas só precisa boa vontade). a verdade é que meus joelhos ainda não estão de todo recuperados das trilhas da Chapada Diamantina e o Fabio ainda sofre um pouco para vestir as botas (hoje ele testou uma tática ninja multimeias e parece que ficou melhor). mas uma coisa de cada vez. bom é conhecer os lugares com calma. (menos Brasília. Brasília é bom de conhecer em duas horas e depois sair correndo de lá.)
