---
title: " a vida na pequena cidade turística"
date: 2016-01-15 09:52:41
---

um pouco é como se ainda não estivesse parada vivendo em um só lugar. acordar cedo por hábito, a manhã livre. o fim de semana ocupado. ir trabalhar e resolver problemas e encontrar guias e fazer reservas em pousadas. voltar tarde. o dia livre e ficar em casa no sofá lendo qualquer coisa. não saber se melhor a chuva que enche as nascentes e os rios ou o céu azul o sol forte que seca finalmente a roupa que passou dias úmida esperando uma brecha pra estar no varal. a chuva que deixa a alma úmida, o sol forte que um calor terrível e o rio seco. o quintal de escalada a vinte minutos, o rio a vinte minutos, a vontade de ficar um pouco mais em casa porque tanta coisa acontecendo e às vezes a gente se sente meio tartaruga.

custa ainda acreditar que estou aqui e posso continuar aqui, e tenho uma mesa de trabalho pra apoiar o computador e organizar minhas manhãs de semana. mas também deixar umas manhãs pra ir escalar, pra dar um mergulho no rio. acostumar-se.
