---
title: "das pausas"
date: 2016-03-21 12:28:00
---

sempre me surpreendo com essas pessoas capazes de parar no caminho de volta à casa e apreciar o pôr do sol depois de dez horas de trabalho dentro do escritório.
