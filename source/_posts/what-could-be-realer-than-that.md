---
title: " what could be realer than that!"
date: 2009-03-29 17:17:16
---

> What do I look for? A work of art, I'll not accept anything less. Yes I know it's shatteringly ingenuous but I wanted to be a painter. They get away with murder in my view; Mr. X. on the _Times_ agrees with me. You don't know how I envy them. They can pick up a Baby Ruth wrapper on the street, glue it to the canvas (in the _right place_, of course, there's that), and lo! people crowd about and cry, "A real Baby Ruth wrapper, by God, what could be realer than that!" Fantastic metaphysical advantage. You hate them if you're ambitious.

Donald Barthelme, _See the Moon?_, do livro "Unspeakable Practices, Unnatural Acts"

[eu devia estar lendo sobre Platão e os sofistas.]
