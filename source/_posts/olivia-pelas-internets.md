---
title: " olivia pelas internets"
date: 2013-08-09 10:01:16
---

porque estou aqui em fortaleza num solzinho camarada de 28 graus (inverno cearense) e com brisa, e entre a correria de sair de são paulo e chegar aqui foi para o ar [meu TOP 5 no blog da Editora Draco](http://blog.editoradraco.com/2013/08/top-5-olivia-maia/), em que conto de cinco livros que de alguma forma influenciaram minha escrita e meus gostos como leitora. também conversei esses dias com o escritor Eduardo Kasse sobre o meu livro _A última expedição_, escrita e pesquisa de campo e meus próximos projetos, e ele [publicou o resultado desse papo no seu blog](http://eduardokasse.com.br/blog/2013/08/07/uma-conversa-com-olivia-maia/). vão lá, leiam, deixem comentários, um rabisco no muro ou sinal de fumaça, que vocês estão todos muito calados ultimamente.
