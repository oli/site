---
title: " turistando com os gringos (gringando em Foz do Iguaçu)"
date: 2014-05-10 15:30:18
---

aqui no hostel em Foz do Iguaçu conheci uma turma legal: um casal de holandeses, um australiano e uma japonesa que cresceu na venezuela. fomos juntos visitar a usina de Itaipu (a Kanna, a japonesa, ainda não tinha chegado nesse dia) e as cataratas do lado argentino, em Puerto Iguazu. decidimos fazer a visita técnica à usina, que é um pouco (bastante) mais cara, mas vale muito a pena. tomamos um ônibus do TTU e descemos na porta da binacional. tínhamos ligado do hostel pra fazer uma pré-reserva do horário da visita. aí era chegar e pagar: 64 reais por pessoa. 

primeiro assistimos a um vídeo que conta a história da usina e explica que cazzo quer dizer esse lance de binacional etc. aí entramos no ônibus com a guia pra seguir pra visita. primero as partes externas, as panorânicas. o tamanho daquele lugaré um troço monstruoso. 

a gente sobe pra ter também uma visão da represa e depois desce pra ficar ao lado daqueles tubos brancos que você vê na foto aí em cima. cada tubo desses recebe a água numa força que é metade da força das cataratas. pouco, né.aí entramos na usina pra conhecer o centro de controle, o corredor gigantesco em que ficam as turbina. o sala de controle fica bem na divisa "simbólica" (já que lá é território dos dois países) entre Brasil e Paraguai, e uma linha no meio marca a fronteira: 

e por último uma parte de uma das turbinas: um troço gigante girando numa velocidade absurda dentro de um buraco com um calor infernal.

no dia seguinte tentamos acordar cedo pra ir pra Puerto Iguazu. no final das contas saímos já passava de nove da manhã. fomos trocar reais por pesos e fomor ao lado do TTU tomar o ônibus que cruza a fronteira. como estava com quatro estrangeiros, precisamos parar na aduana brasileira pra eles declararem saída. aí o motorista nos dá um vale-passagem e vai embora. temos que esperar o próximo ônibus depois das burocracias necessárias. na aduana argentina o próprio ônibus para, todo mundo desce, passa pela imigração etc. o ônibus fica esperando. o cara que me atendeu me olhou meio desconfiado e perguntou -- em mal português -- quantos anos eu tinha. depois que respondi ele virou pro cara do lado: _parece más pequeña, parece quince_. enfim. de Puerto Iguazu pegamos no terminal um ônibus pro parque das cataratas. ufa ufa. chegamos era meio-dia. à primeira vista o parque parece um labirinto. 

perdemos o trem pra Garganta del Diablo mas um funcionário nos disse pra pegar um atalho pra estação seguinte. chegamos bem na hora e entramos no trem. chegando na estação, um caminho de mais ou menos um quilômetro construído por cima do rio leva, em meio a borboletas de todos os tipos e cores, até a principal atração do parque das cataratas argentinas: a visão da Garganta del Diablo.

não dá pra explicar a sensação que é ver aquela água subindo ao longe, o barulho das quedas aumentando, aquele monstro a alguns metros adiante e enfim a visão da garganta aos seus pés. nem sei quanto tempo ficamos ali.

enfim nos arrastamos de volta, bestas. tomamos o trem de volta até a estação do meio para conhecer as outras duas trilhas: a superior e a inferior. seguindo a indicação do dono do hostel, fomos primeiro na trilha superior. quando você pensava que nada seria tão incrível quando a Garganta del Diablo, você descobre que existem mais umas trocentas quedas d'águas, por todos os lados; não acaba nunca. 

a trilha inferior tem uma visão menos grandiosa das quedas, mas ao final se chega num ponto de mirante que fica bem embaixo de uma delas, que é pra se molhar mesmo.

mais fotos no álbum das cataratas de Puerto Iguazu. depois de tanto sobe e desce a gente já estava mesmo muito cansado. voltamos pra entrada do parque, e voltamos pro terminal de Puerto Iguazu. de lá já havia um ônibus de volta pra Foz esperando. o motorista ainda fez a gentileza de parar na aduana brasileira e nos esperar pra voltar. chegamos em Foz com o sol se pondo.
