---
title: inktober com escaladores
photos: 
	- /img/originais/capture000039.jpg
	- /img/originais/capture000040.jpg
	- /img/originais/capture000042.jpg
	- /img/originais/capture000044.jpg
	- /img/originais/capture000045.jpg
	- /img/originais/capture000046.jpg
	- /img/originais/capture000048.jpg
	- /img/originais/capture000049.jpg
	- /img/originais/capture000051.jpg
date: 2020-10-26 09:00:19
---

muitos escaladores e escaladas nesse resto de inktober 2020. usando fotos minhas e de amigos como referência.

não tenho feito TODO DIA mas melhor assim: quando dá, quando quero. se fosse pra ficar estressada não estaria aqui tentando fazer arte livre pra vocês <3

estou sempre transmitindo via [twitch](https://twitch.tv/shadnyex) e depois publicando o vídeo editado em timelapse no [peertube](https://share.tube/video-channels/art_things). quando passar o mês vou montar uma página com todas as ilustrações deste ano de inktober e aviso por aqui, na newsletter e no grupo do telegram.

pra apoiar meu trabalho e ainda receber rabisquinhos todo mês é só colar na minha [página no catarse](https://catarse.me/oliviamaia).
