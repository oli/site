---
title: originais do inktober 2020 disponíveis
date: 2020-11-03 10:47:48
---

atualizei a [página de originais disponíveis](/arte/originais) com as ilustrações do inktober 2020! não estão todas porque duas delas já têm dono, mas logo publico também uma página com os inktobers (todos) digitalizados. quase todos os vídeos de processo [estão no ar](https://share.tube/video-channels/art_things) e pra quem me [apoiar no catarse](https://catarse.me/oliviamaia) até dia 10 de novembro (com uma das opções que recebe ilustrações impressas) os prints do mês vão ser inktobers.

ufa ufa. sobrevivi outubro, quem diria.

agora retomar o ritmo normal de rabiscar e escrever e não surtar. esse ano não vai acabar nunca.
