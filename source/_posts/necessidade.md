---
title: " necessidade"
date: 2009-12-07 20:36:21
---

porque eu gosto demais de escrever essas coisas não-policiais, e brincar com a sintaxe e lapidar as frases até que. e porque eu sei que eu faço direito e agora mais que nunca tenho ainda mais certeza que faço direito. mas também me divirto demais com personagens e tramas policialescas, e tramas com começo e meio e plot points e essa frescura narrativa quase americana. sim, pois. 

preciso muito caminhar rumo a um meio termo, diluir o policial no não-policial e correndo sempre o risco de não acabar fazendo coisa nenhuma ou ainda fazer qualquer coisa que não agrada nem aos leitores de não-policiais e muito menos aos leitores de policiais, e sobro eu com uma pilha gigantesca de livros e ideias e. 

ainda assim, me parece um rumo sincero, um rumo necessário, e muito lentamente porque acredito na literatura de entretenimento, na boa literatura de entretenimento mesmo sem saber tão ao certo o que a faz boa (embora seja mais fácil suspeitar do que a faz ruim). e também porque, afinal, estamos apenas começando, há ainda tempo pra tentar e errar e continuar escrevendo e inventando leitores aqui e ali até que alguma coisa se construa, de um jeito ou de outro, etc.
