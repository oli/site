---
title: " o menino, o lobo e a literatura"
date: 2014-03-18 10:02:35
---

> Between the wolf in the tall grass and the wolf in the tall story there is a shimmering go-between. That go-between, that prism, is the art of literature.

li no site brain pickings um artigo sobre a visão do Nabokov a respeito da escrita e da arte de contar histórias. ele afirma que a literatura nasceu no momento em que aquele menino veio correndo da colina gritando "o lobo! o lobo!" e não havia lobo nenhum. que o menino fizesse isso muitas vezes e acabasse comido pelo lobo, quando ele apareceu de verdade, não vem ao caso aqui. o que vale é a invenção: o _shimmering go-between_ determina a arte da literatura. esse devir, esse "e se". aquilo que a gente já sabia?

> Going back for a moment to our wolf-crying woodland little woolly fellow, we may put it this way: the magic of art was in the shadow of the wolf that he deliberately invented, his dream of the wolf; then the story of his tricks made a good story.

o que também me faz lembrar daquele conto do Guimarães Rosa: "Os três homens e o boi dos três homens que inventaram o boi". literatura pura. o boi inventado acaba por sobreviver aqueles que o inventaram. a mágica da arte está na possibilidade construída na "sombra do lobo" inventado pelo menino. na sombra do boi dos três homens (que inventaram o boi). ainda sobre isso, dando um sutil passo ao lado, também no site brain pickings li sobre [Tolkien e suas opiniões a respeito dos contos de fadas](http://www.brainpickings.org/index.php/2013/12/05/j-r-r-tolkien-on-fairy-stories/). vale a leitura. pra além de afirmar que não existe "literatura pra crianças" e que contos de fadas só são infantis na medida em que os "adultos" os leem pras crianças, ele aponta a relação da linguagem com a invenção, a partir da capacidade da nossa mente de usar a linguagem pra criar abstrações.

> The human mind, endowed with the powers of generalization and abstraction, sees not only green-grass, discriminating it from other things (and finding it fair to look upon), but sees that it is green as well as being grass.

essa capacidade da mente de distinguir a "coisa" da sua "característica" é o que também permite que se façam trocas: se eu penso grama e penso verde, também posso pensar grama e vermelho, sangue e azul:

> The mind that thought of light, heavy, grey, yellow, still, swift, also conceived of magic that would make heavy things light and able to fly, turn grey lead into yellow gold, and the still rock into a swift water. If it could do the one, it could do the other; it inevitably did both. When we can take green from grass, blue from heaven, and red from blood, we have already an enchanter’s power — upon one plane; and the desire to wield that power in the world external to our minds awakes.

um lobo que não existe, um boi inventado, coisas pesadas que se tornam leves e podem voar. a insistência de um poder inventivo do qual, aparentemente, a gente não consegue fugir.
