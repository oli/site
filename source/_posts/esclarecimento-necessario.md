---
title: " esclarecimento necessário"
date: 2013-03-04 22:28:25
---

não sou burra. tenho lá minhas limitações, como qualquer um, mas não é burrice esse não compreender das atitudes e das coisas mais comuns do mundo e da sociedade civilizada etc (e digo _coisa_ como único substantivo possível em sua estúpida imprecisão). muito da minha não-compreensão é na verdade o não-sentimento de verdade; não o sentimento de não-verdade, vê lá. são coisas diferentes. é o saber intelectual desvinculado da apropriação do sentimento; compreender tão somente por esforço empático que há uma lógica por trás desse ou daquele comportamento, mas sentir que essa lógica não me pertence e jamais poderia me pertencer. porque é esse despertencimento. tem-se de habitar o mundo para se compartilhar o hábito. e às vezes de criança a gente se habitua mesmo é a ficar olhando as coisas pelo lado de fora. (esclarecimento esse provavelmente desnecessário.) mas esse esforço.
