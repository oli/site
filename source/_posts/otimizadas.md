---
title: otimizadas as imagens todas
date: 2020-08-10 10:13:08
---

porque `optipng` ou converseen não estavam resolvendo isso de otimizar os arquivos .png da [página de ilustrações](arte/rabiscos) e o bicho tava virando um **monstro**. alguns programas online faziam isso de reduzir o número de cores pra otimizar o arquivo mas ainda assim era trabalho demais pro tipo de coisa que eu precisava.

reencontrei o plugin "save for web" do gimp, que vinha como parte do programa antes e eu não encontrava mais. estava ali no AUR esperando por mim. FUNCIONA lindamente, nisso de transformar um arquivo png-24 em png-8, que para web é bem suficiente, ainda mais porque eu disponibilizo a versão em alta resolução toda inteira pros interessados com um clique de distância. mas fazer um por um, vá pa porra, né.

encontrei um programinha que faz exatamente isso, então: converte png-24 pra png-8 perdendo o mínimo possível de qualidade: `pngquant`. também tem no AUR. basicamente cortei pela metade o peso da página de ilustrações. agora carrega mais rápido e serve às paciências mais curtas.

aliás, tem coisa nova que acabei de publicar. agora, teoricamente, vai ter toda semana. [corre lá](arte/rabiscos).
