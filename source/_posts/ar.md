---
title: mais ilustrações no ar
date: 2020-07-23 17:01:49
---

tem ilustração nova na [página de ilustrações](/arte/rabiscos/)! quase todas as novas foram feitas durante streamings de domingo em [twitch.tv/shadnyex](https://twitch.tv/shadnyex). costuma começar 10h30 e você pode pedir um dos originais, que eu mando pelo valor mínimo da taxa de envio dos correios.

claro que pode também entrar na página aqui do site e baixar tudo de graça, e imprimir, e botar na parede. e também pode [entrar em contato](mailto:olivia@oliviamaia.net) pra me contar o que fez com os downloads!
