---
title: "coisas novas e chocantes"
date: "2019-07-03 17:51"
photos:
  - /img/rabiscos/img/2019_002.png
  - /img/rabiscos/img/2019_003.png
  - /img/rabiscos/img/2019_004.png
  - /img/rabiscos/img/2019_005.png
---

quatro [novos rabiscos](/arte/rabiscos/) disponíveis pra [fazer download ou pra comprar versão impressa](/arte/rabiscos/). e quem comprar qualquer coisa na loja até 19 de julho ganha um brinde surpresa. lhes parece?

## telegramz

fiz um [canal no telegram](https://t.me/rabiscologia) pra divulgar toda novidade que surgir aqui pros meus lados. a vantagem de canal é que dá pra silenciar as notificações e você ainda recebe tudo no conforto do seu bolso. a depender do feedback de vocês vou ajustando o conteúdo com o tempo, postando mais ou menos coisas, enfim. e quem sabe se houver interesse a gente não pode também fazer um grupo pra conversar sobre literaturas e rabiscologias de vez em quando. porque twitter tá muito chato mesmo, né.

pra acessar: [t.me/rabiscologia](https://t.me/rabiscologia).
