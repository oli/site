---
title: " correndo atrás do sol"
date: 2015-03-31 22:36:23
---

porque foi descer do ônibus em cabo frio às oito da manhã da última quinta-feira e saber que pelo menos parte do que a gente estava procurando a gente já tinha encontrado: o calor. aí ônibus de linha pra arraial do cabo e parada no camping. um camping caro e bem sem vergonha, mas é o que tinha. nossa barraca nova é grande e parece uma casa por dentro. tem até espaço sobrando. a guardar a comida e tudo. até compramos um kit de frescobol pra ocupar espaço lá dentro. pena que durante o dia não dá pra ficar dentro dela porque: calor. 

mas durante o dia a gente vai pra praia mesmo. 

duas noites atrás teve uma chuva na madrugada com vento e violência e eu nem dormi porque parecia que a barraca ia colapsar a qualquer momento. às quatro da manhã a chuva parou, a barraca estava só um pouco molhada por dentro perto da porta e nas bordas, a gente ainda estava seco e finalmente dormi um pouco (até o sol cretinamente sair e fazer a barraca virar um forninho outra vez). aí que vamos bem por aqui nos preparando pra algo que ainda não sabemos o que é e buscando lugares pro caminho e praticando frescobol que obviamente é muito mais divertido que fazer abdominais (e deixa dolorido igual).
