---
title: " contos e biscoitos de polvilho"
date: 2016-06-04 22:09:43
---

me meti aqui com a leitura de uns contos da Lydia Davis e do Juan José Millás, assim simultaneamente, a norte-americana e o argentino; livros do tipo _collected stories_ e _articuentos completos_, do tipo que, segundo as palavras de Juan José Millás, são ruins para ler na cama mas podem servir como travesseiro; ambos infinitos feitos de pequenos contos de uma ou duas páginas, todos hipnotizantes; contos para ler um atrás do outro como se come biscoito de polvilho um atrás do outro sem se dar conta de que se está enchendo a cara de biscoito de polvilho, com a diferença que dessa vez se trata de uma bolsa gigantesca de biscoito de polvilho.
