---
title: " ... and we're back"
date: 2014-12-05 18:56:11
---

ops. tivemos um probleminha aqui com o domínio (ops ops) mas já está tudo de volta ao normal. vou retomar os relatos, que na verdade não são muitos, porque foram uns dias meio estranhos (pra não dizer que foi uma montanha-russa). vou acabar escrevendo mais sobre tudo isso na newsletter que deve sair, pra variar, com atraso. mas é que, né. não tá fácil. já estou em Mina Clavero, no começo do caminho rumo ao sul, digamos. estou apaixonada por esse lugar e não quero ir embora nunca mais. que é quando você desce na rodoviária e rola amor à primeira vista, não sei explicar (na verdade já fui me apaixonando enquanto ainda no ônibus, na estrada). talvez por aqui esteja minha casinha. passarei natal e ano novo na cidade de Mendoza com a mama e depois sigo ao sul da província e enfim Neuquén, e a imensa Patagônia. se tiver férias entre janeiro e fevereiro e quiser me encontrar pr'aqueles lados me avisa.
