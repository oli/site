---
title: " começar"
date: 2010-03-21 23:12:59
---

tento começar uma narrativa de aventura. nada parece certo. o início descritivo e chato, um começo direto e confuso (que planeta é esse, o que são esses seres?) ou ainda um retrospecto cansativo de eventos recentes e a falta de memória do protagonista.

tudo parece errado.  

tenho pelo menos duas histórias desse gênero. uma que é minha e outra que bolei há muitos anos com um amigo do colégio, que me deu o thumbs up para assumir o texto.

me confundo no narrador. quem conta essa história? por onde começar? para quem ele está contando tudo isso?

por onde COMEÇAR?

a história está pronta. queria começar. que não para assumir esse projeto agora, mas para fazer dele um projeto paralelo. mas como é difícil começar.
