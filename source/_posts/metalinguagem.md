---
title: " metalinguagem"
date: 2010-03-04 01:12:48
---

> Estamos pois na vizinhança de uma questão à qual parecem ligadas a literatura e a arte em geral: se não há poema que não tenha por "assunto", tácito ou manifesto, sua realização como poema, e se o movimento do qual provém a obra é aquilo com vistas a que a obra é por vezes realizada, por vezes sacrificada.

Maurice Blanchot _O livro por vir_
