---
title: " sierras de Córdoba: Valle Hermoso e arredores"
date: 2014-08-30 14:57:01
---

na verdade acabei ficando por Valle Hermoso mais tempo do que eu pretendia. porque de repente não estava mais com vontade de seguir adiante para o norte e Bolívia e Peru. de repente tinha vontade de parar um pouco e precisava pensar no que fazer dos próximos meses. Valle Hermoso fica perto de um povoado um pouco maior chamado La Falda, numa área chamada Valle de Punilla (a oeste das Sierras Chicas). toda a região é turística, as serras de Córdoba, e os argentinos vêm de todos os lados pra cá no verão pra curtir os balneários dos rios. porque isso de ir pra lugar frio nas férias é coisa de brasileiro. os argentinos vão é atrás do calor. 

por aí conheci La Falda (fica colada em Valle Hermoso) e um dia saímos para caminhar num lugar chamado Vaquerías, que tem algumas trilhas e modestas quedas d’água (num fim de semana de calor inesperado coisa de 30 graus em inverno cordobês). meu amigo Luciano num dia desses me levou pra dar uma volta de carro pros lados de Cosquín, dei umas voltas por Villa Giardino, La Cumbre, Los Cocos, pela comuna de Casa Grande e comecei a considerar seriamente viver pr’aqueles lados.

também visitamos San Marcos Sierras, no norte do vale, um povoado de rua de terra cheio de hippies e portenhos com um riozinho _hermoso_. outro lugar bom de morar. ali perto tem também Capilla del Monte, outro ponto turístico bastante _de moda_ e por isso também um pouco mais caro.

aproveitando também pra escrever um conto pra mandar pra uma coletânea de Sherlock Holmes, da Editora Draco. e fiz uma tatuagem com um tatuador genial, único tatuador de La Falda, agradável surpresa. recomendo. 

aliás, por falar em verão inesperado os últimos dias que estive aí foi como se a primavera chegando fora de hora. trinta graus, trinta e três. ainda não sabia (ainda não sei) que fazer com os próximos meses. em Villa Carlos Paz me esperava a Gracinha, amiga da família do meu pai, que me ia emprestar um apartamento que ela aluga por temporada.
