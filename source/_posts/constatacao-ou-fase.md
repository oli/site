---
title: " constatação ou fase"
date: 2009-06-04 01:16:42
---

o linux me curou dessa busca incansável pelo próximo programinha que faz isso ou aquilo, a próxima webapp que faz aquilo ou o que mais. talvez seja o caso de desassinar ainda mais outros feeds de blogs de tecnologia e webcoisas, que já pouco me dizem sobre o que poderia me interessar. descoberta: o mundo não se implode quando deixamos para ver email só duas vezes por dia. ou quando desligamos o computador antes das nove da noite. ou quando isso significa ter que ligar ele outra vez antes de dormir porque esqueci de reler o email do professor de diálogo platônico. _getting better all the time. getting so much better all the time._ \[eu já falei que está frio?\]
