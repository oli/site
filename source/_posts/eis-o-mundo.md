---
title: " eis o mundo"
date: 2009-06-28 19:30:38
---

> The political role of rhetoric arouses Plato's suspicion and criticism. He asks why the Athenian democracy should regard rhetorical ability as a sufficient qualification for giving political advice, and he presents two objections against rhetoric: (1) If, as Isocrates admits, the orator does not try to reach independent rational convictions of his own on moral and political questions, he will simply repeat popular prejudices. If he simply follows the ignorant and prejudiced moral and political assumptions of the majority, his advice will not promote the common good. (2) If the orator persuades people, not because he convinces them that the course of action he advices will really benefit them, but because he arouses their feelings and prejudices, even against their better judgment, what he persuades people to do will not even be what they really want to do.  
> 
> T. H. Irwin, "Plato: the intellectual background". _In: The Cambridge Companion to Plato_

qualquer semelhança com o presente será mera coincidência. ... [?] [ou](http://lyricwiki.org/Pato_Fu:O_Mundo_N%C3%A3o_Mudou).
