---
title: " bolívia, dia três: potosí"
date: 2011-07-19 03:58:10
---

eu disse 3800m? bueno, me enganei. são 4300m de altitude por aqui. diz que é a cidade mais alta do mundo.

saímos cedo de sucre e ao terminal de buses num táxi que parecia uma carroça. dez horas já partia o ônibus para potosí. três horitas de viagem que na verdade quatro e meia rumo ao topo do mundo; súbito que eis o altiplano, do jeito que promete o nome. rogério me cutuca porque eu já ia num cochilo de dramin.

ainda não vi nenhuma lhama. só um monte de porco e ovelha.

potosí parece uma cidade pela metade, em construção. andamos um pouco, mas o fôlego curto. almoço e descanso. devagar não se sente tanto o soroche, mas rogério teve dor de cabeça e mandou ver nas aspirinas. eu estou bem boazinha, só mei sem fôlego e com o nariz seco que haja rinosoro.

depois andamos mais, caçamos umas frutas e jantar. já comemos todo tipo de pizza nessa viagem: pizza de rua (really; picante o sin picante? dios), pizza pra turista, pizza boa com queijo de cabra, pizza safada num lugar muito verde limão e lotado de potosinos. oy, e até agora nenhum pollo.

um frio maldito nas ruas. sentamos num banco da praça e olhamos a gente passar.

mais tarde no hostal conversamos um teco com uns brasileiros (bati um papo mui breve com um coreano mas logo a internet voltou e ele me esqueceu completamente depois de erguer os braços e dizer que 'oh, internet, internet!'. sério. aí um documentário sobre as minas de cerro rico.

bueno. lavar o rosto na água mais gelada do UNIVERSO e dormir.
