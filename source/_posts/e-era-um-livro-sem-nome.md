---
title: " e era um livro sem nome"
date: 2009-01-08 06:06:55
---

que terminei de escrever, e fiz uma revisão primeira. botei fingido no formato pocket que eu vou querer imprimir com a fonte mais ou menos do tamanho que deveria ser e deu por cima umas 310 páginas. é policial. enfim finalmente Pedro Rodriguez meu investigador do DHPP virou gente e sobreviveu uma história inteira. a investigação começa porque morreu um primo do delegado, e foi dito que suicídio. mas não foi suicídio. e o delegado já avisa no primeiro capítulo que não quer a corregedoria na história, e a corregedoria aparece mesmo assim, atrás de um dos investigadores plantonistas que atendeu ao chamado do suicídio. isso assim por cima, porque nunca sei bem como fazer sinopse de livro nenhum. muito menos quando fui eu que escrevi. e o livro continua sem nome. só tem nome a primeira parte. a segunda parte não tem. o livro mesmo, que é bom, menos ainda. pensei assim em qualquer coisa com o mês de agosto (agosto já usaram, vai dizer), ou com cozinheiros. pensei também em "segunda mão" mas depois me chutei. vou continuar pensando.
