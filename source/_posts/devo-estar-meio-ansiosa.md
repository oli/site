---
title: " devo estar meio ansiosa"
date: 2009-10-27 18:46:07
---

devo andar meio ansiosa, pelo tanto que minha perna mexe incessantemente quando fico sentada. um terror não ter o que escrever no blog, fico pensando que fazem falta as aulas na letras, me dava assunto, etc. vai saber. na educação é tudo uma merda, fora as aulas de psicologia que servem pra alguma coisa -- servem pra eu descobrir o quanto sou neurórita; o professor _ah porque isso só em casos de neurose grave_ e eu _ahhh, sim, claro por que não_.

e ganhei um netbook, um eeepc 1005ha mui simpático, e apesar do teclado em inglês que custa a se reacostumar (porque pra quem já digitou num mac que não sabia o que eram dead keys, nada pode ser impossível) estou achando o bichinho uma lindezinha. a bateria dura oito horas e diz que está crítica quando chega a uma hora e eu pensando larga de ser fresco, meu notebook com uma hora está com mais da metade da carga total.

já meti um ubuntu netbook remix nele, porque ninguém merece aquele windows xp com 50 ícones na bandeja do sistema e aberrações similares sistema operacional adentro. sem contar que parece que o vídeo fica melhor no ubuntu. ou é só que o ubuntu é mais bonito mesmo, então. 

e não que o UNR esteja livre de bizarrices, porque esse aplicativo que maximiza as janelas não conversa mui bem com qualquer um, andou brigando com o quodlibet e faz sumir umas janelas de vez em quando. no mais, wifi funciona, câmera funciona e o teclado é gostosinho e faz tec tec tec.

devo estar meio ansiosa, não consigo deixar quieta a perna quando sento, e há trabalho pra fazer, muito trabalho pra fazer, então, alright.
