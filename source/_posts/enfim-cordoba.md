---
title: " enfim Córdoba"
date: 2014-08-29 14:53:17
---

gripe gripe gripe. cheguei em Córdoba era sábado, 19 de julho de manhã, e foi descer do ônibus estava minha amiga ali na porta com um sorriso e _tu!_ já me tirou mochila das costas, marido levantou a mochila grande e quase de braços dados me levaram até o carro. acho que me senti tão em casa que a gripe me atacou com tudo. Caro me cuidou tipo mãe brava. melhor recepção impossível. claro que também Córdoba é cidade grande, mas caminhamos pelo parque Sarmiento e não vou dizer que verde porque nesse inverno fica tudo meio terra. um senhor frio. 

durante o dia fazia um solzinho e eu podia ficar um pouco na frente da casa, junto do Tomás, o cachorro, que inclusive dormia comigo na cama e me fazia companhia quando Caro e Jorge estavam trabalhando. nos dias seguintes eu fiquei foi de cama com febre, mas pelo menos estava em casa e entre amigos!

na sexta-feira já praticamente boa tomei o ônibus pra Valle Hermoso com outra amiga, que trabalha em Córdoba mas mora em Valle Hermoso. amigos todos que conheci também em 2012, nessa minha outra viagem à Argentina.
