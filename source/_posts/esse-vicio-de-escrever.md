---
title: " esse vício de escrever"
date: 2015-03-10 15:03:54
---

existe um prazer físico em escrever com o teclado do computador que faz despertar umas vontades de escrever simplesmente por estar usando as teclas, corrigindo um texto alheio, digitando uma atualização no twitter. esse vício de apertar as teclas com as pontas dos dedos e ver as palavras formarem-se na tela, quase alheias ao movimento que as produz. e continuar digitando, escrevendo, inventando; qualquer coisa só pra não parar de mover os dedos, de escutar esse ruído tão familiar e tão lindo, esse ruído que faz surgir personagens e histórias e tudo isso que teoricamente não existe de verdade mas que, enfim, nunca se sabe.
