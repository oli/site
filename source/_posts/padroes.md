---
title: " padrões"
date: 2013-12-04 21:01:57
---

aí você passa um ou dois dias tomando banho no rio, usa a mesma camiseta a semana inteira, as unhas estão sempre com terra e aquela mancha de barro na sua perna passa o dia todo ali sem você nem perceber. talvez seja que na cidade as coisas são limpadas higienizadas arrumadas organizadas com mais frequência e fervor porque na cidade as coisas são tão mais sujas e desordenadas.
