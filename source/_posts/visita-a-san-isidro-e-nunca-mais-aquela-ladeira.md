---
title: " visita a San Isidro e nunca mais aquela ladeira"
date: 2014-10-28 08:00:45
---

vou contar o que acontece quando um ser humano das terras baixas do mundo passa três noites num povoado a 2780 metros de altitude cuja inclinação natural é tipo uns 83 graus no sentido mais comprido das ruas e além disso esse ser humano está hospedado a uns dez metros do ponto mais alto do lugar tanto que a vista da varanda é quase a mesma da vista do mirador um pouco mais acima. e mais: dito ser humano inventa de caminhar um monte todos os dias até o rio e até a feira e até qualquer coisa e subir e subir e subir e óbvio que existe qualquer tipo de distorção digna de um desenho do Escher porque eu tenho certeza que nesse lugar se sobe muito mais do que se desce. se bem que meu joelho diria o contrário, porque ele sofre é com a descida, e eu já estava num nível joelho -10 nesse último dia inteiro que tínhamos em Iruya. pois: estávamos todos _hecho mierda_, como se diz por aqui. nos despedimos de Juan que ia um pouco mais ao sul antes que acabassem suas férias;

e seguimos as três a San Isidro, um micro-povoado que fica a mais ou menos uns sete quilômetros de Iruya caminhando pelo leito do rio seco. 

(mais ou menos seco.) 

e um caminho lindo de quebrada, rodeado por montanhas altas e coloridas. muitas vezes precisamos cruzar o rio quando nos restava seguir o caminho dos carros e aí era buscar a combinação de pedras mais interessante pra não sair rolando rio abaixo. 

e: subida. são talvez menos de dois quilômetros em descida até o encontro do rio Iruya com o rio San Isidro, e depois dali em diante só subida. os últimos quinhentos metros ainda uma senhora subida, quando já se vê no topo do morro algumas casinhas. aos pés do povoado perguntamos a um rapaz por onde a gente subia e ele mui engraçadinho _ali tem um elevador_ e né. uma escadaria inclinada com um monte de degraus irregulares. mas a gente fica reclamando e ali atrás vinham uns três homens trazendo material de construção e uma privada, tudo no ombro.

no final das contas fizemos todo o caminho em duas horas. parecia que tínhamos caminhado um dia inteiro. ali em cima não chega carro, por motivos óbvios, e as ruas são como corredores (na verdade um caminho pela esquerda subia e outro à direita fazia uma curva; em frente tinha mais morro). um cachorro nos observava de uma porta. 

uma senhora passou e perguntamos pela _doña_ Teresa e suas mundialmente famosas empanadas de queijo. passamos pelo que era a igreja e a escola e adiante uma fileira de _comedores_ e hospedagens. a última era a de Teresa.

aterrizamos numa das mesas e pedimos uma dúzia de empanadas de queijo e meia dúzia de carne. porque se é pra fazer errado, faz errado direito. 

informo que: melhores empanadas de queijo do universo.

depois caminhamos mais um tanto, subimos um pouco mais de morro e tiramos fotos até que o frio e o vento nos expulsaram daqueles picos muito altos e achamos melhor buscar um lugar pelo rio pra descansar um pouco. e nem isso: o céu tinha fechado, ameaçava chover e vinha por todo o caminho um vento gelado.

acabamos parando quando quase o encontro dos rios. uma pausa de poucos minutos e seguir de volta. depois de um monte de descida, era pra terminar o dia com mais um monte de subida. miles de subida. chegar à igreja de Iruya e olhar pra cima e lá no fim da rua a hospedagem de _doña_ Asunta, distante. nem conseguimos comprar comidas pra noite porque um monte de lugares fechados. subimos e subimos e subimos e capoft. e depois no começo da noite baixar outra vez pra comprar frutas e bolachinhas pro café da manhã e subir de novo, e subir pela última vez, e não subir nunca mais.
