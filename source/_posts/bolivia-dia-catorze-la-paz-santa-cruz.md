---
title: " bolívia, dia catorze: la paz, santa cruz"
date: 2011-07-30 04:17:38
---

capow.

28 cafiaspirinas;  
6 anti-inflamatórios;  
6 relaxantes musculares;  
29 dramins;  
2 neosaldinas.

agora no aeroporto de santa cruz muito oxigênio muita umidade muito calor. voamos num puma.

desistidos de tihuanaco porque o cansaço, e disseram que andou chovendo um tempo atrás e o que se haveria pra ver está mais enterrado do que deveria estar, mais ruína do que deveria ser. pena pena. salimos a andar mais um pouco, comprar regalitos e afins, comer comida o menos boliviana possível o menos frango frito possível o menos pimentão possível.

que quinze dias parece tão pouco mas tão muito. primeira semana foi mais apressada e porque uyuni estava todo cheio de água acabamos fazendo o tour mais rápido e sobrando dias para la paz. o bom foi visitar copacabana e a ilha do sol com mais calma. descansar da correria dos primeiros dias.

de o livro encontrado e personagem encontrado. personagens. encontrei mais do que esperava encontrar, que é quando as coisas se encaixam desse jeito meio assustador. esses povoados perdidos no altiplano eram tudo que a narrativa precisava. coqueza nos pés de tunupa e esse mundo de gringos de todo canto.

que eu tinha pensado em criar uma correria pelas ruas de la paz mas fico pensando agora que o protagonista não vai durar um quarteirão. capaz nem o gringo que deveria estar fugindo, e está na altitude há umas cinco ou seis semanas.

o que não deixa de ser útil à trama.

porque pisei em santa cruz veio aquela umidade e a caminhada até o desembarque pareceu tão fácil. mal estar que vinha me acompanhando fugiu, ufas. estômago nem reclamou com o jantar.

agora esperar passar o tempo pro voo pra são paulo, às 4h40 da manhã.
