---
title: " a impossibilidade do relato"
date: 2014-07-27 10:01:15
---

> ... it is impossible to convey the life-sensation of any given epoch of one's existence, -- that which makes its truth, its meaning - its subtle and penetrating essence. It is impossible. We live, as we dream -- alone...
>
> _Heart of Darkness_, Joseph Conrad

que também é a dificuldade de Riobaldo em _Grande sertão: veredas_; a mesma impossibilidade do relato, da precisão dos detalhes que realmente importam quando o que realmente importa sequer se parece assim no momento do acontecer:

> Contar é muito dificultoso. Não pelos anos que se já passaram. Mas pela astúcia que têm certas coisas passadas — de fazer balancê, de se remexerem dos lugares.

ou porque não existe o passado senão como percepção de passado, ilusão de memória, e essa constatação da impossibilidade do relato nada mais é do que a constatação do óbvio.

> Aquele encontro nosso se deu sem o razoável comum, sobrefalseado, como do que só em jornal e livro é que se lê. Mesmo o que estou contando, depois é que eu pude reunir relembrado e verdadeiramente entendido — porque, enquanto coisa assim se ata, a gente sente mais é o que o corpo a próprio é: coração bem batendo.

"coração bem batendo" seria a "life-sensation", "its subtle and penetrating essence" na linguagem do Conrad (na fala do Marlow). o "razoável comum" seria esse retomar de fatos, aparar as bordas, limpas as partes sujas (como diria aquele vídeo do filtro solar que por uns anos circulava pela internet feito gripe no metrô de Buenos Aires no inverno) e remendar as partes quebradas para apresentar ao público: contar em jornal ou livro. ou porque a insistência em compartilhar a experiência é uma das coisas que nos faz humanos. e a impossibilidade de compartilhar a experiência o que reforça essa humanidade, porque frustrados buscamos a arte, a literatura, a música e mais tantas quantas alternativas de compartilhar pelas bordas isso que não pode ser compartilhado por inteiro.
