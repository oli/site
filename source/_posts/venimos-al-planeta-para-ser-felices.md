---
title: " venimos al planeta para ser felices"
date: 2012-12-08 17:00:11
---

Pepe Mujica, presidente do Uruguai, no Rio+20: http://www.youtube.com/watch?v=zsOGZKRVqHQ

> (...) tenemos que darnos cuenta que la crisis del agua y de la agresión al medio ambiente no es la causa. La causa es el modelo de civilización que hemos montado. Y lo que tenemos que revisar es nuestra forma de vivir.

quando tempo dura o planeta quando todos tiverem o mesmo padrão de vida de um americano médio? ou, como disse o presidente, quando os indianos tiverem a mesma média de carros por família que têm os alemães? quando eliminarmos a pobreza para dar a todos no planeta um espaço igual na sociedade de consumo? não se engane: você e eu estamos aqui com nossos computadores e nossa internet rápida porque milhares de pessoas não têm nada. nosso sistema econômico só funciona porque há gente na miséria. tudo o que você consome e joga fora é às custas da fome alheia. assim funciona o nosso sistema. não se trata de dar aos pobres o padrão de vida dos ricos. trata-se de rever a sociedade que queremos, a cultura que queremos, o padrão de vida que queremos, o sistema econômico que queremos. trata-se, afinal, de repensar o que queremos. trata-se de parar de tentar comprar a felicidade, para usar um clichê bem safado. não é fácil, não vai ser fácil, não tem como ser fácil. mas ninguém está imune e ninguém é inocente. o discurso do presidente uruguaio é um beliscão num monstro gordo e com os músculos todos dormentes, mas, che! já é tarde.
