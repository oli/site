---
title: na área
date: 2023-07-24 10:25:16
host: cuscuz.in
username: oli
toot: "110769417560412203"

---

![página de quadrinhos. 1: um campo aberto. um homem em primeiro plano mexe numa câmera fotográfica e ao fundo duas pessoas agachadas ao lado de um corpo caído enquanto um investigador observa. 2: o investigador observa e o fotógrafo tira foto de uma bola de futebol. 3: o homem observa uma investigadora tomando notas numa prancheta. 3: o investigador pergunta ao legista "que te parece?". 4: o legista diz "rapaz..."](/img/quadrinhos/caiunaarea1.jpg)
![página de quadrinhos, continuação. 1: um corpo caído no chão, os pés de chuteiras. texto: "foi na área". 2: a cara do investigador. texto: "sim...". 3: plano aberto, de cima. estão num campo de futebol, na área. o corpo caído junto à pequena área enquanto o fotógrafo conversa com a investigadora e o investigador e o legista observam. texto: "é pênalti."](/img/quadrinhos/caiunaarea2.jpg)

coisa séria!
