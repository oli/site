---
title: " rabugices de nabokov"
date: 2016-03-12 19:08:01
---

> (...) let me say at once that I reject completely the vulgar, shabby, fundamentally medieval world of Freud, with its crankish quest for sexual symbols (something like searching for Baconian acrostics in Shakespeare’s works) and its bitter little embryos spying, from their natural nooks, upon the love life of their parents.

_Speak, Memory_, Vladimir Nabokov
