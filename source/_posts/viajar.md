---
title: " viajar"
date: 2014-04-22 12:00:12
---

viajar: estar em constante movimento é esquecer um pouco o passado imediato, perder a “noção do tempo”, como se diz, como se houvesse um padrão de “noção” que a gente devesse guardar numa caixinha. também deixar de pensar demais no futuro, quando o agora nos ocupa o suficiente e futuro é o jantar, a hora de dormir, o dia seguinte. é olhar fotos de um mês atrás e não se reconhecer ali; refazer-se a cada dia, a cada instante. desfazer-se em mil instâncias de um “eu” fragmentado que se espalha ao longo dos dias e das horas. é mudar com a paisagem, com a situação, com o espaço. é ser o espaço e depois deixar de ser o espaço anterior, e assim sempre, adiante. olhar para trás como quem relê uma narrativa antiga da qual não se participou.
