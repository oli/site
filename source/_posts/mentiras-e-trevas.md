---
title: " mentiras e trevas"
date: 2014-07-26 11:55:42
---

> There is a taint of death, a flavour os mortality in lies, - which is exactly what I hate and detest in the world - what I want to forget.
> 
> _Heart of Darkness_, Joseph Conrad
