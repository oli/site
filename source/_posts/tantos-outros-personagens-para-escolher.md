---
title: " tantos outros personagens para escolher"
date: 2016-04-03 16:56:30
---

enquanto não publico a newsletter já obviamente atrasada, deixo com vocês (e principalmente aos amigos e amigas escritores) [um artigo longo e interessante](http://electricliterature.com/in-search-of-the-novels-first-sentence-a-secret-history/) pra pensar um pouco em nosso terrível ofício (enquanto vou lendo _On Writing_, do Stephen King, que tenta me empurrar num estado de ânimos contrários ao da leitura deste artigo).

> Between advertising and the novel, the lines of influence are blurry because the new marketplace was blending the forms together, forcing them to change their practices to survive. Every advertiser was a novelist, every novelist, at least for a sentence, an advertiser. Or, as a critic for _Life_ in 1913 describes the work of one first sentence, a street “barker.” (...) As Marchand writes, America was now for the first time a majority urban population. For the first time in human history, the majority of people the average person encountered in a given day were strangers. Others had no inner lives; they were just the external characteristics visible during a first impression. For advertisers, this meant cajoling people about every detail of their appearance, littering advertisements with scrutinizing eyeballs. And there’s something of this too for the novel-reading public. Adrift in what the era’s writers continually described as a “flood” of fiction, there was no time to create a character _ab ovo_; he or she must come fully formed, must offer quick and memorable impressions. There are so many other characters to choose from.

» [In Search of the Novel First Sentence | Electric Literature](http://electricliterature.com/in-search-of-the-novels-first-sentence-a-secret-history/)
