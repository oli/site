---
title: " o livro ausente"
date: 2010-11-30 22:18:27
---

> O livro ausente, formatado e arquitetado no pensamento, pode manter-se como reserva de si mesmo -- uma realidade virtual inesgotável -- e nem chegar à condição de objeto.

Daisy Turrer (sobre _O livro por vir_, de Maurice Blanchot, no prólogo de _O livro e a ausência de livro em Tutaméia, de Guimarães Rosa_)
