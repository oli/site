---
title: " bolívia, dia 1: sucre"
date: 2011-07-17 04:40:08
---

uma horita de atraso no voo pra sucre. vinte e cinco minutos de voo pelos andes e o pouso no meio do nada.

e pensando que o mundo em volta tão enorme, não vou encontrar meu personagem nunca. ou: o desespero do protagonista com essa noção de infinitute. nada nunca.

o hostel é uma lindezinha. la dolce vita.

depois o futebol e meu deus como esses gringos bebem. fomos com eles à feria de alacita, una feria de cositas pequeňas e comprei dinossauros e uma lhama. e os gringos continuavam bebendo. voltamos eu e rogério já mei tontos de andar e de altitute (e a cerveza paceňa durante os jogos deve ter ajudado um pouco) e os gringos foram a um karaoke pra continuar bebendo.

bueno. friozinho simpático, por ora.
