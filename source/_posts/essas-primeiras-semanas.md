---
title: " essas primeiras semanas"
date: 2013-09-01 16:12:53
---

mais uma quarta-feira e são quatro semanas desde que saí de são paulo para não voltar mais. agora escrevo de jericoacoara, enquanto ouço esse vento sem fim que mora aqui pelo litoral do ceará, olhando o camping do seu natureza pela janela desse quartinho anexo em que estamos hospedados. escrevo do celular porque o computador ficou na casa do meu pai, em fortaleza. hoje, domingo, decidimos ficar em jeri mais uns dias, porque embora o quarto vá estar ocupado, seu natureza nos deu um desconto camarada para ficar em barraca nos próximos dias. o camping aqui é muito tranquilo e não dá muita vontade de ir embora. 

porque afinal nenhuma pressa e depois passar o feriado com meu pai e seguir para o piauí: serra da capivara. pode dizer que não caiu a ficha, essa coisa de perceber que não precisa mais voltar para são paulo. acho que eu estava já muito preparada, muito certa. ficou sendo uma coisa natural. estar deitada no banco da mesa no lado de fora da cozinha depois de inventar umas pizzas caseiras com uma argentina; sentar no chão pra ler debaixo das árvores; desenhar enquanto o grupo de artesãos trocam materiais e ouvem música. penso que olha aqui onde eu estou mas sim, claro, como poderia ser diferente. conhecer tanta gente que viaja assim e faz disso a vida e por que não? viver com pouco é muito possível (até mesmo estando em jericoacoara), só depende de saber de que se está disposto a abrir mão.

escrever ainda só umas páginas. encontrei uns cenários e tive umas ideias para um dos romances que comecei a escrever. aos poucos vou voltando a desenhar, que era uma coisa que eu estava bem com vontade de fazer. o estojo é um peso que vale a pena nas costas. agora é ver o quanto do que está na mochila é mesmo necessário e quem sabe se livrar de algumas mudas extras de roupa pelo caminho. e descobrir o que vem.
