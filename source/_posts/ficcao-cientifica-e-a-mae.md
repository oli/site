---
title: " ficção científica é a mãe"
date: 2010-06-09 17:15:31
---

terminei de ler A invenção de Morel, do Adolfo Bioy Casares. tinha ouvido alguém dizer -- quando me viu lendo o livro -- que era ficção científica. como eu estava só nas primeiras páginas, minha resposta foi: não me conta mais nada que quero ler o livro sem saber coisa nenhuma.

(o que pode ser melhor do que ler um bom livro sem todo o peso da crítica e da teoria em cima dos seus ombros?)

agora quis continuar lendo mesmo depois de o oftalmologista me dilatar as pupilas e tornar meu olhos esquerdo astigmático (ou astigmatístico?) um tanto quanto inútil pra se enxergar de perto, e meu olho direito míope um tanto quanto inútil pra se enxergar de longe e ambos os olhos com os óculos terrivelmente inúteis pra enxergar qualquer coisa a menos de um metro de distância. logo: sem óculos, numa distância calculada, tapando vez ou outra o olho esquerdo pra descansar a vista, meu olho esquerdo doendo por causa desse colírio do demônio etc. mas quis continuar lendo, porque pela metade do livro já sabemos muito mais do que o protagonista sugere saber e nos resta esperar pra saber e agora, e agora, e agora?

o posfácio do Otto Maria Carpeaux, nessa edição da Cosac, é um troço lindo, e eu não poderia dizer mais nada sobre o livro depois dele. de qualquer forma, esteja dito que funciona assim, como posfácio, e é como mais uma pedra de certeza de que você acabou de ler uma narrativa que, como você desconfiava, era assim tão simples e ao mesmo tempo tão terrível, e os defensores do gênero que me desculpem, mas não há muito ali de ficção científica.
