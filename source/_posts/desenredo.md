---
title: " desenredo"
date: 2010-03-08 17:19:15
---

me vem a literatura pelos poros. escrevo mentalmente, trechos de livros inexistentes que jamais existirão, trechos que se perdem tão logo são formulados, formulações que me escapam. vêm num momento de semi-vigília, quase-sono ou nem isso. os pensamentos que tomam forma em palavras, estruturas sintáticas que se desenredam e se criam sozinhas. escrevo meus pensamentos, mentalmente, como quem escreve em segredo um livro impublicável.
