---
title: " a vida dá voltas"
date: 2016-12-27 10:00:00
---

> Enquanto estiver na rotunda não estou perdido, pelo menos não volto atrás. E eis um dos atractivos daquela circulação, circulação quase infinita não fora ela terminar com exactidão às trezentas voltas: em redor de uma rotunda ninguém volta atrás, ninguém se engana, ninguém tem de assumir o erro e fazer inversão de marcha. A vida, apesar de tudo, é fácil. Numa rotunda.

Gonçalo M. Tavares, _Matteo perdeu o emprego_
