---
title: " bolívia, dias cinco e seis: uyuni e salar de uyuni, parte 2"
date: 2011-07-22 04:39:14
---

porque o sinal aqui é edge, internet jegue, e 3g só em cidade grande. facebook só rezando.

uyuni de dia é mui simpática, com esse jeito de cidade do velho oeste que têm as cidades e povoados aqui no altiplano. passamos a praça para comprar uns iogurtes e bolachas pra comer no hostal, depois arrumar a mochila pequena pro salar e rumar à agência.

dez e meia: digamos que onze hora. reunido o grupo nos despedimos de outros quatro brasileiros recém-chegados na cidade e fomos conhecer o jipe e nosso guia-motorista, pedro.

e a holandesa louca, que completou o grupo de última hora.

primeiro as paradas mais burocráticas: cemitério de trens e um povoado cujo nome me fugiu, pra comprar artesanatos e fazer o turista.

por todo lado no altiplano se vê casinhas abandonadas, povoados abandonados ou semi-abandonados. porque o frio, e a agricultura e gado que dependem de chuvas às vezes inexistentes. as famílias abandonam suas casas, mas cobrem a porta e as janelas com pedras, que é uma forma de dizer que vão voltar. também nos entornos de uyuni.

então o salar. rapaz, não tem muitas palavras pra esse troço. branco, infinito. el salar mas grande del mundo, disse pedro, cheio de orgulho. e isso só o começo, essas primeiras horas. ver os caras extraindo o sal e subir nos morrinhos. take crazy pictures, i love crazy pictures, disse a holandesa louca, já no cemitério de trens.

e porque a neve, e o salar tinha água, como não acontece há anos no inverno. seguimos no jipe bem lentamente porque o sal devora o motor. parada pro almoço no hotel de sal, com direito a carne de lhama e quinua, tudo muito gelado porque o vento FRIO SOCORRO. menina, calça underwear, calça de fleece e calça de flanela. por cima camiseta underwear, manga curta dry fit, fleece e jaqueta corta-vento. tudo bem joia mesmo, não fossem os dedos gelados e as orelhas que insistiam em fugir da proteção.

e só o branco na distância. todos listos e rumo ao alojamento aos pés do vulcão tunupa.

a montanha seguia na nossa frente, fugindo sempre. o salar com água reflete o céu feito espelho; de um lado nublado e do outro a imensidão azul que já vinha nos acompamhando. é sal que não tem fim. duas horas de viagem avançando sem nenhuma estrada. o pico do vulcão de neve e um tom de cobre. a cordilheira dos andes fronteira com o chile à esquerda.

o alojamento ficava no povoado de coqueza, numa das pontas do salar. tomamos um lanche e fomos dar uma volta, bater um papo com as lhamas (elas tem um jeito todo blasé de te olhar depois de pegar do chão um pouco de mato e fazer voar um tanto em você por causa do vento) e com os flamingos e ficar besta com a vista do salar infinito refletindo o céu feito fosse o mar num dia manso.

o jantar custou um pouco a sair. mas veio com vinho argentino e tudo. um dos nossos chegou meio atrasado porque foi ver o pôr do sol e depois que caiu a noite não achou o caminho de volta. trouxe boas notícias: o céu só das estrelas. fácil que fomos lá fora passar frio e ver o que paulistano nem mais sonha em ver. vale todo o frio, o pé gelado, o vento.

e banho nada, nunca. só vestir meias secas e dormir dentro do saco para os graus negativos da madrugada.

segundo pedro, chegou a -8. deu pra ficar com calor. duro foi dormir com a secura do ar e o soroche, que desde potosí não sei o que é dormir a noite toda. consegui pregar o olho só quando fiquei mezzo sentada, com um dos cobertores enrolados nas costas. aí acordar bem cedo pra ver nascer o sol.

ele ameaçou, mas a fome gritou mais forte e voltamos pro refeitório.

o jipe subiria até um ponto da montanha e o resto pra subir a pé. múmias numa gruta no caminho, de civilização pré-inca. que medo meu deos. segui caminho atrás do carioca do grupo, que estava todo pimpão me atirando um troço branco atrás de uma das muretas de pedra empilhada (essas muretas por todo o lado na encosta da montanha). era neve.

neve neve, toda branquinha e consistente, de pegar e modelar com a mão. e quem ia dizer que eu ia ver neve pela primeira vez na bolívia? a holandesa louca já  chegou atirando neve nos outros, depois de descobrir por que o bando de brasileiros estava tão agitado com um troço que ela deve ver todo ano. os gringos que passavam também olhavam como se fôssemos todos malucos.

mas arriba, si? cada dez passos já parecia que a gente ia morrer. minhas pernas me odiando muito. fôlego zero. o fflch e a holandesa louca foram na frente e sumiram da vista. rogério ficou pelo caminho, o resto do nosso grupo também, um tanto mais acima. queria demais continuar subindo, mas todos que passavam voltando diziam que pra ver de cima o vulcão mais uma hora. nunca.

esperar os dois fortes, ver as fotos e voltar ao jipe. muertos.

antes do almoço fui com o fflch dar uma volta no povoado de coqueza. é que já encontrei metade do meu livro por aqui. tão melhor do que eu jamais poderia imaginar. tudo pronto pra uso. mal precisa agitar ou adicionar água.

e água meu deus, ÁGUA. já de cima da montanha se via quase todo o salar, todo espelho do céu, de não saber onde era terra e onde era céu. azul e branco. no jipe, rumo a uma das ilhas, tinha hora que era como estar flutando nas nuvens. os andes refletidos faziam imagem de caledoscópio. do topo do mundo ao meio do nada.

na ilha mais caminhada, mais sal, mais imensidão branca. seis mil cactos. escalar um pico safado de uns trinta metros era pra morrer sem fôlego. 3600m de altitude, que é menos que potosí, mas com um esforço fisico dobrado. deos. chegou hora de ir embora meu sonho era banho e cama. aliás, meu sonho ainda é banho e cama, que agora escrevo no bus (semi-cama! com cobertor! com banheiro!) pra la paz chacoalhando numa estrada de terra que não vai acabar nunca. agora mesmo o motorista passando a dois por hora por causa de uns buracos enormes no caminho.

a holandesa louca e o carioca seguiram pra potosí (acabou o bloqueio), o fflch vai precisar passar a noite em uyuni porque só tem ônibus pra villazón de manhã e as duas meninas seguem aqui com a gente pra la paz.

mas o salar de uyuni só as fotos, e quem sabe assim se pode ter uma vaga ideia da estupidez que é natureza quando resolve impressionar os olhos. e as fotos, buena, só quando eu voltar. agora são nove e meia da noite. deixa tomar outro dramin e tentar cochilar um pouco. ainda que capaz esse post só possa ser postado daqui umas horas, quando voltar o sinal da tigo, que pegava no caminho pro topo da montanha, mas pelo jeito não pega entre uyuni e oruro.
