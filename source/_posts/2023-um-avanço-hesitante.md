---
title: um avanço hesitante
date: 2023-03-26 08:56:51
---

> Este termo, hesitante, parece-nos fundamental. Um avanço hesitante: eis um método; avançar, não em linha recta mas numa espécie de linha exaltada, que se entusiasma, que vai atrás de uma certa intensidade sentida; avanço que não tem já um trajecto definido, mas sim um trajecto pressentido, trajecto que constantemente é posto em causa; quem avança hesita porque não quer saber o sítio para onde vai – se o soubesse já, para que caminharia ele? Que pode ainda descobrir quem conhece já o destino? Hesitar é um efeito da acção de descobrir; só não hesita quem já descobriu, quem já colocou um ponto final no seu processo de investigação. “As minhas dúvidas formam um sistema”, escreveu Wittgenstein.

Gonçalo M. Tavares, _Atlas do corpo e da imaginação_.
