---
title: " iruya é uma festa, parte impossível"
date: 2014-10-27 19:44:34
---

_em que eu me encontro numa festa de pueblo com um grupo de cumbia eletrônica e meia dúzia de gato pingado dançando numa quadra poliesportiva_ porque veja bem, à tarde acabaram os festejos para a virgem. isso eu não sei bem o que era; sei que: era aniversário do Juan que quis jantar no hotel e depois ainda quis pagar pra gente os 50 pesos cada um da entrada da baladinha que começava às 22h na quadra poliesportiva da escola. era, suponho, motivo pra fazer festa. mas vai vendo que teve até carimbo no punho e polícia fazendo revista na entrada. aí que tinha um grupinho de meia dúzia de pré-adolescentes perto do palco e mais alguns espalhados nas bordas. Marie comprou uma cerveja e Juan uma coca-cola e se meteram no meio da quadra pra dançar.

o grupo uns três cantando e uns seis tocando, meio invisíveis, enquanto os três primeiros dançavam em sincronia e em looping infinito e alternavam vozes graves e um falsete meio sinistro que aparentemente faz parte da coisa. 

a verdade é que mui divertido que Marie e Juan e Barbara dançando indiferentes ao fato de serem os únicos naquela quadrinha sem vergonha enquanto a banda animada tocava como se para uma multidão de 500 pessoas.

mas depois a mistura de cumbia com música eletrônica e eu me sentei porque não tem mais joelho e estava quase dormindo. quando fui embora a quadra estava até que metade ocupada e a turma se divertindo, e ainda quando saímos um pouco vinha uma galera subindo a ladeira na maior animação. quase uma da manhã e Iruya estava praticamente acordando da siesta pra começar a festa.
