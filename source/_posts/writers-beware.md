---
title: " writers beware"
date: 2010-05-17 15:02:14
---

> Hemingway's greatest influence was on hardboiled detective writers like Dashiell Hammett, who actually did some good work. Then Chandler came along with private eye Philip Marlow, more poached than hardboiled. Chandler wrote entirely too well, stacking up bizarre similes and metaphors like so many poker chips in a high-stakes game of roulette in some lost casino of the soul. So to speak. Not until Elmore Leonard would crime fiction finally free itself of Chandler's self-conscious style.

sobre os [10 romances mais perigosos pra quem quer se tornar escritor](http://thetyee.ca/Books/2010/05/14/TenHarmfulNovels/). muito bom. _são geralmente bem escritos, mas seus efeitos foram desastrosos: inspiraram jovens escritores a imitá-los_. confesso que já tentei imitar o Chandler. é irresistível. mas nunca consegui. _stacking up bizarre similes and metaphors like so many poker chips in a high-stakes game of roulette in some lost casino of the soul_. puro Raymond Chandler. so to speak. perigosíssimo.
