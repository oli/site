---
title: " lei de murphy aplicada à cidade pequena"
date: 2018-03-11 16:06:51
---

**1ª lei:** quando chove, cai a internet;  
**2ª lei:** quando chove forte, cai a energia, o sinal de celular e o sinal de telefone fixo;  
**3ª lei:** o gás sempre acaba no domingo (na hora do almoço).
