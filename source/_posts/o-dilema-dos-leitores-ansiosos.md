---
title: " o dilema dos leitores ansiosos"
date: 2016-09-02 14:15:11
---

porque há tanta coisa pra ler que encarar um livro de mais de quatrocentas páginas parece às vezes um exagero, um privar-se de outros livros, talvez mais interessantes, quem sabe? mas passei um mês trabalhando pra montar curso de redação e só agora dois meses depois terminei de ler um livro que era pra ter me tomado alguns dias então, né, que diferença faz?

o leitor de ebook indica o tempo que o livro tomaria de leitura: seis horas. não é tanto assim. e confesso que nem tinha tanta vontade de ler esse tal de Salman Rushdie tão cedo por motivos de: cara de azedo homem branco etc. mas li sobre esse "Two Years, Eight Months and Twenty-Eight Nights" e fiquei curiosa, e comecei a ler, e decidi que gostei do que li. decidi também principalmente que não tenho nenhuma obrigação de respeitar a minha própria fila de leituras se o livro que eu quero ler agora surge de repente atrás de um tanto de fumaça sem fogo e puf. confesso também que o livro me ganhou um pouco na epígrafe do Ítalo Calvino (traduzida obviamente pro inglês porque o livro é em inglês):

> Instead of making myself write the book I ought to write, the novel that was expected of me, I conjured up the book I myself would have liked to read, the sort by an unknown writer, from another age and another country, discovered in an attic. -- Italo Calvino

também obviamente o leitor ansioso agarra assim uma empreitada um pouco (nem tanto) mais ambiciosa depois de uma fase excepcionalmente ansiosa de leituras curtas e não por isso deixa de contar os minutos -- mesmo se o livro for ótimo -- pra próxima leitura dessa pilha imensa e crescente, sempre crescente. Salvar
