---
title: " desses posts que não fazem sentido"
date: 2011-03-08 14:35:00
---

das coisas que não podem ser ditas.

não tanto porque a consciência impeça e mais por inexistência de palavras que capazes de traduzir uma sensação muito vaga que é alegria e desespero, ao mesmo tempo, e mais que isso é a busca por essa sensação muito vaga que se esconde nas curvas mais inesperadas do tempo e que nunca se pode reter. sempre algo a se dizer substituído por conversa à toa e comentários sobre o tempo. é a certeza da existência dessa alegria desespero e a certeza da impossibilidade de apreendê-la.

é transformá-la em outra coisa: trechos de livros, ou os relevos distantes na lateral de uma estrada ou altos e baixos em playground de criança numa praça no meio da cidade.

é gastar palavras que inúteis se amontam sem na verdade dizer coisa alguma.

ou.
