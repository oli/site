---
title: " eu não gosto de cinema"
date: 2010-03-08 05:29:55
---

sim.

assisto a um filme ou outro. gosto de alguns. poucos, entre os poucos que assisto. me encanto com outros, assisto mais de uma vez. gosto de Indiana Jones, dos filmes do Harrison Ford e do John Cusack etc. gosto de uns filmes bobinhos e engraçadinhos. gosto de filmes sobre escritores. mas quase sempre porque assisto por acaso. por pura preguiça de levantar do sofá.

não faço de cinema uma obrigação. não planejo cinema. não tenho vontade de interromper meu dia e assistir a um filme. sentar à noite e _agora vou ver esse filme_. existem exceções. mas são exceções. e mesmo assim, se o filme sai de cartaz e não vi, a vontade passa. só sentar e esperar que a vontade passa.

não gosto de cinema. não acho que preciso ver esse ou aquele filme porque a cultura e a arte. não acho que cinema seja arte. alguns filmes, talvez. alguns. mas nem por isso me sinto obrigada a assistir nenhum deles. assim como gosto de arte mas odeio museus. quero que a cultura do cinema vá pro inferno. coisa de quem tem preguiça de ler. não tenho paciência. nenhuma. não consigo. pensar em cinema me dá um tédio terrível.

cinema tem seus méritos, mas prefiro a literatura. parece que estou comparando coisas que não se podem comparar, eu sei. mas são tantos livros, que dispenso o cinema. quase sempre, vou dispensar. 

não importa o quanto você me diga que esse filme é um clássico e eu tenho que ver, porque a narrativa, o simbolismo, a crítica, o mundo, fuu. ou que era um clássico que eu tinha que ter visto. quero que exploda. se eu calhar de ver, ok. mas não fode. não me venha dizer que eu sou inteligente e devia conhecer a cultura porque um clássico, a arte, a literatura. não fode, hein. por favor.
