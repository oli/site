---
title: " de volta a lençóis"
date: 2015-12-17 13:00:28
---

o ônibus sai às 10h15 desta quinta-feira. chegamos em Lençóis na sexta-feira à tarde, casados, passaporte já devidamente carimbado pela polícia federal e RNE devidamente requerido, com o resto da casa nas costas e a ladeira de calçamento irregular da rua das Pedras pra subir. ainda não temos internet em casa, mas vamos providenciar assim que a gente chegar. agora começa.
