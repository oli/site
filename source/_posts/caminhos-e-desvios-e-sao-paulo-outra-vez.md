---
title: " caminhos e desvios e São Paulo outra vez"
date: 2013-12-22 12:00:27
---

vou acabar chegando em São Paulo antes do que o previsto, ou ainda bem no tempo previsto, e que depois tinha sido desprevisto. aproveitei que vim parar em Uberaba na casa da minha prima, e que ela ia voltar para o natal e ano novo; carona e companhia e economizar porque alta temporada não é amiga dos bolsos mochileiros. fico ainda uma semana ou dez dias de janeiro antes de voltar para Minas: Serra da Canastra, nascente do São Francisco, Lagoa Santa, cidades históricas... esses rumos. o itinerário exato ainda não sei, que vou descobrir melhor quando faltar alguns dias para ir embora. por enquanto vejo se convenço algum amigo com carro a aproveitar possíveis férias e ir comigo para a Serra da Canastra no começo de janeiro. sem carro não sei se animo. alguém aí está na pilha? aí São Paulo passear um pouco, rever amigos, resolver pendências e ficar à toa também. quem tiver um tempo para matar, manda mensagem! chego no dia 24 de para-quedas nos natais das (minhas) famílias.
