---
title: "das máscaras"
date: 2014-12-22 18:30:07
---

> “Persona” quiere decir máscara, y cada uno de nosotros tiene muchas. ¿Hay realmente una verdadera que pueda expresar la compleja, ambigua y contradictoria condición humana? Me acuerdo de algo que había dicho Bruno: siempre es terrible ver a un hombre que se cree absoluta y seguramente solo, pues hay en él algo trágico, quizá hasta de sagrado, y a la vez de horrendo y vergonzoso. Siempre, decía Bruno, llevamos una máscara, que nunca es la misma sino que cambia para cada uno de los lugares que tenemos asignados en la vida: la del profesor, la del amante, la del intelectual, la del héroe, la del hermano cariñoso. Pero ¿qué máscara nos ponemos o qué máscara nos queda cuando estamos en soledad, cuando creemos que nadie, nadie, nos observa, nos controla, nos escucha, nos exige, nos suplica, nos intima, nos ataca? Acaso el carácter sagrado de ese instante se deba a que el hombre está entonces frente a la Divinidad, o por lo menos ante su propia e implacable conciencia.

_La resistencia_, Ernesto Sabato
