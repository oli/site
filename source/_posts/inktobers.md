---
title: primeiros dias de inktober
photos: 
	- /img/originais/capture000026.jpg
	- /img/originais/capture000031.jpg
	- /img/originais/capture000032.jpg
	- /img/originais/capture000033.jpg
date: 2020-10-04 19:48:40
---

aí os primeiros rabiscos de inktober de 2020.

HUMANOS.

porque faz tempo que não faço humanos, né. humano é um bicho difícil de desenhar, porque a gente acha demais que sabe como um humano tem que ser. como a ideia do inktober é praticar, resolvi escolher um 'tema' difícil.

tenho feito _lives_ no twitch em [twitch.tv/shadnyex](https://twitch.tv/shadnyex) geralmente depois do almoço, tipo umas 14h (com excessão do domingo, em que a live costuma rolar por volta de 10h) quase todos os dias agora em outubro pra mostrar o processo. depois transformo a gravação em um vídeo time-lapse que [publico na minha canal de rabiscos do peertube](https://share.tube/video-channels/art_things).

vai ter um tanto de coisa nova este mês, então. já viu as [novas ilustrações](/arte/rabiscos) e a [página de originais disponíveis](/arte/originais)? veja veja. e veja também minha [página de apoio recorrente no catarse](https://catarse.me/oliviamaia). e pode sair espalhando por aí.
