---
title: "voltar a ler Camus"
date: 2016-09-06 15:28:13
---

um objetivo: escrever com a mesma leveza com que Albert Camus escreve sobre a peste que toma a cidade de Oran, que acaba com as esperanças e com o amor, que faz imperar a indiferença e nada acontece além do tempo e das mortes e algumas conversas desinteressadas. (enquanto isso me pergunto por que, depois da obrigação de ler _O primeiro homem_ pra disciplina de Didática na faculdade, demorei tanto para voltar a ler a ficção de Camus; embora eu saiba que um pouco isso se deve ao que há de sedutor e maravilhoso nos seus ensaios, que vez ou outra me ocupavam nos últimos anos.)
