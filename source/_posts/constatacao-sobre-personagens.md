---
title: " constatação sobre personagens"
date: 2013-01-27 16:25:16
---

que todo personagem precise de algo de obsessão, ou medo, ou neurose extrema e desespero, que coincida ou não com a motivação para narrativa, mas que de qualquer forma participe dela ou a modifique, a dificulte, a impeça até o ponto em que se é impossível voltar atrás ou desistir.
