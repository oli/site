---
title: campanha de financiamento para meu novo livro
date: 2023-06-13 14:11:07
---

finalmente está no ar a [campanha de financiamento coletivo pro lançamento de NORTE](https://www.catarse.me/norte_olimaia), meu novo romance.

![card da campanha de financiamento: NORTE, por Oli Maia.](/img/posts/card-13.png)

a sinopse:

> No espaço entre Buenos Aires, os desertos de Catamarca e a Patagônia aos pés da cordilheira andina, Téo Miranda está deslocado de seu território habitual. Acostumado a investigar adultérios na São Paulo urbana, o detetive particular descobre-se em paragens remotas, contra o vento e a poeira, na busca por um tio desaparecido há mais de trinta anos.
> 
> Marcada por curvas imprevistas e a aridez da Ruta Nacional 40, a investigação se torna ainda mais difícil pela companhia insistente de Gustavo, com quem Téo mantém um relacionamento incerto. O detetive logo se dá conta de que para encontrar o tio precisa compreender seus motivos, mas a tarefa o obrigará a enfrentar suas próprias inseguranças — ou correr o risco de se perder nesse eterno meio de caminho no rumo do fin del mundo. 

a campanha dura mais ou menos dois meses mas quem puder apoiar logo eu agradeço demais, porque apoio chama apoio e ajuda na divulgação e no alcance do projeto <3

também agradeço quem puder ajudar com divulgação: pode espalhar pra quem você achar que vai se interessar! me ajuda a fazer um barulhinho?
