---
title: integração com o fediverso
date: 2023-08-10 15:49:36
host: cuscuz.in
username: oli
toot: "110866942926906152"
---

adaptei [este código](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/) com a ajuda da adaptação que [Veronica Olsen](https://berglyd.net/blog/) usou no blog dela e botei comentários do fediverso nos posts deste blog. não é um sistema de comentários propriamente dito: na verdade você responde ao toot em que eu divulguei o post e a resposta (se for pública) aparece aqui embaixo.

o sistema é manual e depende de eu inserir no arquivo do post o _id_ do toot depois, mas fica bonitinho e serve de arquivo pras conversas geradas pelas publicações do blog. 

agora só falta eu escrever no blog.
