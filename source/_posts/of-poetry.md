---
title: " of poetry"
date: 2009-03-09 04:50:26
---

> Poetry is not a turning loose of emotion, but an escape from emotion; it is not the expression of personality, but an escape from personality. But, of course, only those who have personality and emotions know what it means to want to escape from these things.

T. S. Eliot, _Tradition and the Individual Talent_, 1920.

[sim, eu já postei isso antes, but it never gets old.\] e o fim do ensaio:

> There are many people who appreciate the expression of sincere emotion in verse, and there is a smaller number of people who can appreciate technical excellence. But very few know when there is expression of _significant_ emotion, emotion which has its life in the poem and not in the history of the poet. The emotion of art is impersonal. And the poet cannot reach this impersonality without surrendering himself wholly to the work to be done. And he is not likely to know what is to be done unless he lives in what is not merely the present, but the present moment of the past, unless he is conscious, not of what is dead, but of what is already living.
