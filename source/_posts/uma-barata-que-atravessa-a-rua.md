---
title: " uma barata que atravessa a rua"
date: 2009-11-10 05:29:25
---

curioso que se note logo ali no meio da rua uma barata que meio perdida não sabe bem se segue até a calçada ou espera cessar o ruído

         (e baratas ouvem ruídos?)

         dos carros que passam por cima, sem parar, porque a rua dessas pequenas artérias da cidade que entopem com certa facilidade, duas faixas estreitas e a terceira dos ônibus, e a barata.

e confesso que quase torcendo por uma roda que passasse por ela e (porque baratas esses bichos nojentos); mas nada. mais outro carro e ainda a barata ali, tonta como convém, para lá e para cá sem na verdade ir a lugar nenhum, sem sair do meio da rua, sem se mover o suficiente para que mais certo uma roda e. parecia absurdo que a barata ali, viva, e os carros.

e porque do absurdo se foge adiante rua abaixo, convinha a mim atravessar a rua, deixar passar alguns carros por cima, indiferentes, indiferente.

os céus vigiam os tolos, mas que se pode haver de tolo em uma barata

               (porque baratas esses bichos nojentos); baratas esses bichos que certo estarão ainda habitando a terra quando nós. certo, porque atravessam ruas, inconsequentes e os carros, nada, ninguém.
