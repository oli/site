---
title: " bolívia, dia dez: copacabana e la paz"
date: 2011-07-26 13:51:21
---

voltar de barco a copacabana, almoçar com as meninas que foram conosco a uyuni, despedir-se outra vez, tomar o ônibus na plaza sucre com um dos motoristas gritando lapazlapazlapaz. última viagem de ônibus.

la paz me derrubou, por qualquer motivo. seja essa alteração abrupta de altitude que a geografia da cidade permite. saímos pra andar um pouco depois de arrumar hostal, mas não deu pra rodar muito. melhor era dormir. escrevo de manhã porque à noite capotei.

aqui o sinal é bom, mas ainda não achamos tomada pra regarregar as coisas e a bateria do celular está no fim. vamos ver com o funcionário do hostal. buena. já estou bem melhor, mas vou sossegar um tanto pra ficar bem boa pro chacaltaya. hip hip.
