---
title: " parada estratégica na última cidade (quase) grande antes do Chuí"
date: 2014-06-02 19:00:29
---

a ideia de parar em Rio Grande (também poderia ter sido Pelotas, mas enfim) era pegar encomenda enviada pela mãe nos correios, ir atrás de banco pra sacar dinheiro e resolver cartão de crédito novo e sem senha pra poder usar no exterior. tudo resolvido nesta segunda-feira e ainda com a companhia da Renata que tinha que mandar fazer uns óculos. na volta paramos no Bar do Beto e comprei a passagem pro Chuí: sete horas da manhã de terça-feira. a Renata topou acordar meia hora mais cedo, já que ela tem que estar na faculdade umas 7h30, e me levar até o ponto pra pegar o ônibus. fim de semana fez um frio úmido e chuvoso: 

aí noite de domingo o céu limpou e a temperatura caiu (cinco graus tá bom pra você?). na segunda amanheceu sol e gelado. céu sem nuvens. belo dia pra umas burocracias. mas bueno! tudo resolvido, passagem comprada. amanhã antes do meio dia estou no Chuí e atravesso a fronteira pra ficar uns dois dias com um casal do couchsurfing que mora em San Luis al Medio, um povoado de uns 600 habitantes a uns 30km dali. capaz fique uns dias sem internet, pelo menos até arrumar um chip pré-pago uruguaio pra usar no meu celular. hasta pronto!
