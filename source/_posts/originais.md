---
title: sobre os envios de originais
date: 2021-02-22 12:18:12
---

eu fico tão feliz de poder disponibilizar minha arte assim de forma livre, com download gratuito e licença creative commons, e mais feliz ainda porque eu posso disponibilizar os originais aos interessados e cada um paga o que pode -- e todo mundo pode ter um pedacinho de arte, mesmo quando só pode cobrir os custos de envio. 

eu fico feliz porque isso funciona, tem funcionado lindamente; e entra um dinheiro pra ajudar com as contas e a comida dos gatos. 

sério mesmo; é uma alegriazinha. enorme.

cultura livre é muito amor.
