---
title: terminei um livro e acho que comecei outro
date: 2022-05-23 08:58:47
---

porque aí retomei a escrita de NORTE e de repente não conseguia mais parar de escrever.

![gráfico de contagem de palavras](/img/posts/stats.png)

então terminei o livro.

ainda falta o trabalho de revisão, mas estou esperando feedback de alguns leitores beta. enquanto isso, curiosos corajosos podem espiar o livro [por aqui](https://norte.oliviamaia.net).

então aproveitei a pilha e comecei a escrever mais um.

ops.
