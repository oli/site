---
title: auto-ajuda para as crises existenciais
date: 2022-03-16 09:36:04
---

de Johan Deckmann. [via](https://www.thisiscolossal.com/2022/03/johan-deckmann-books/).

![capa de livro emoldurada: how to be truly happy almost once a year](/img/posts/deckmann-4-1831x2048.jpg)
