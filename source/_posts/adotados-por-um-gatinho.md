---
title: " adotados por um gatinho"
date: 2016-01-17 13:57:24
---

numa noite em que eu estava trabalhando ele apareceu pra pedir comida. suíço deu leite e pôs de novo pra fora. duas noites depois ele apareceu outra vez. demos leite e botamos pra dentro. no dia seguinte ele já tinha cama, ração pra filhote, caixa de areia. dei vermífugo e cortei as unhas (ele nem reclamou). já sabe usar a areia e já sabe escalar a cama pra se acomodar entre os travesseiros quando a gente não está olhando. falta um nome. estamos [aceitando sugestões](http://oliviamaia.net/contato/).
