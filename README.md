# site pessoal de Olivia Maia.

[oliviamaia.net](https://oliviamaia.net)

🏠

---

[Hexo] website using GitLab Pages.

----

Forked from @VeraKolotyuk

[ci]: https://about.gitlab.com/gitlab-ci/
[hexo]: https://hexo.io
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[hexo-server]: https://hexo.io/docs/server.html
